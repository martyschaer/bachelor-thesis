package dev.schaer.vc.verifier

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.journeyapps.barcodescanner.ScanContract
import com.journeyapps.barcodescanner.ScanOptions
import dev.schaer.mrzreader.api.DocumentScanContract
import dev.schaer.mrzreader.model.DocumentData
import dev.schaer.vc.model.certificate.VaccinationCertificate.MAGIC_BYTES_AS_STRING
import dev.schaer.vc.verifier.databinding.FragmentInitialBinding

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class InitialFragment : Fragment() {

    private var _binding: FragmentInitialBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private var _certScanResult: String? = null

    private val scanDocumentLauncher =
        registerForActivityResult(DocumentScanContract()) { doc: DocumentData? ->
            if (doc != null && _certScanResult != null) {
                val bundle = Bundle()
                bundle.putString(CERT_SCAN_RESULT, _certScanResult)
                bundle.putParcelable(ID_SCAN_RESULT, doc)
                findNavController().navigate(
                    R.id.action_InitialFragment_to_VerificationResultFragment,
                    bundle
                )
            }
        }

    private val barcodeLauncher = registerForActivityResult(ScanContract()) { result ->
        if (result.contents != null && result.contents.startsWith(MAGIC_BYTES_AS_STRING)) {
            _certScanResult = result.contents
            scanDocumentLauncher.launch(false)
        } else if (result.contents != null) {
            // unable to find vaccCERT QR-Code, retry
            scanQR()
        } else {
            // cancelled
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentInitialBinding.inflate(inflater, container, false)

        if(arguments?.getBoolean(SCAN_IMMEDIATE) == true) {
            scanQR()
        } else {
            IssuerRevocationList.init(requireContext())
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.buttonScanCertificate.setOnClickListener {
            scanQR()
        }

        binding.buttonGateMode.setOnClickListener {
            startGateMode()
        }


    }

    private fun startGateMode() {
        GatewayChannel.setup(requireContext())
        if(GatewayChannel.INSTANCE.ensureConnected()) {
            Toast.makeText(context, "Gateway Mode", Toast.LENGTH_SHORT).show()
            findNavController().navigate(R.id.action_InitialFragment_to_GatewayScannerFragment)
        } else {
            Toast.makeText(context, "Unable to connect to gateway companion", Toast.LENGTH_LONG).show()
            Log.e(tag, "Unable to connect to gateway")
        }
    }

    private fun scanQR() {
        val options = ScanOptions()
        options.setBeepEnabled(false)
        options.setDesiredBarcodeFormats(ScanOptions.QR_CODE)
        options.setPrompt("Scan the certificate QR code")
        options.captureActivity = CertificateCaptureActivity::class.java
        barcodeLauncher.launch(options)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}