package dev.schaer.vc.verifier

import com.journeyapps.barcodescanner.CaptureActivity
import com.journeyapps.barcodescanner.DecoratedBarcodeView

class CertificateCaptureActivity: CaptureActivity() {
    override fun initializeContent(): DecoratedBarcodeView {
        setContentView(R.layout.certificate_capture)
        return findViewById(R.id.custom_barcode_scanner)
    }
}