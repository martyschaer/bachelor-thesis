package dev.schaer.vc.verifier

const val CERT_SCAN_RESULT = "cert_result"
const val ID_SCAN_RESULT = "id_result"
const val SCAN_IMMEDIATE = "scan_immediate"