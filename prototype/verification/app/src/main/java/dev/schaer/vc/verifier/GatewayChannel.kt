package dev.schaer.vc.verifier

import android.content.Context
import android.util.Log
import org.eclipse.paho.client.mqttv3.MqttClient
import org.eclipse.paho.client.mqttv3.MqttConnectOptions
import org.eclipse.paho.client.mqttv3.MqttException
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence
import java.util.*

class GatewayChannel private constructor() {
    private val emptyPayload = byteArrayOf(0x42)

    private lateinit var qrTopic: String
    private lateinit var idTopic: String
    private lateinit var resultTopic: String
    private lateinit var options: MqttConnectOptions
    private lateinit var client: MqttClient

    companion object {
        lateinit var INSTANCE: GatewayChannel

        fun setup(context: Context) {
            INSTANCE = GatewayChannel()
            INSTANCE.init(context)
        }
    }

    private fun init(context: Context) {
        val props = Properties()
        val inputStream = context.assets.open("application.properties")
        props.load(inputStream)

        qrTopic = props.getProperty("mqtt.topic.qr")
        idTopic = props.getProperty("mqtt.topic.id")
        resultTopic = props.getProperty("mqtt.topic.result")

        val mqttHost = props.getProperty("mqtt.host")
        val mqttUser = props.getProperty("mqtt.user")
        val mqttPass = props.getProperty("mqtt.pass").toCharArray()

        options = MqttConnectOptions()
        options.isAutomaticReconnect = true
        options.isCleanSession = true
        options.userName = mqttUser
        options.password = mqttPass
        options.connectionTimeout = 5

        client = MqttClient(mqttHost, "scanner-${UUID.randomUUID()}", MemoryPersistence())
    }

    fun ensureConnected(): Boolean {
        if(!client.isConnected) {
            try {
                client.connect(options)
            } catch (e: MqttException) {
                Log.e("GatewayChannel", "Unable to connect", e)
                return false
            }
        }
        return true
    }

    fun showQR() {
        client.publish(qrTopic, emptyPayload, 1, false)
    }

    fun showId() {
        client.publish(idTopic, emptyPayload, 1, false)
    }

    fun sendScanResult(qrContents: String, idNumber: String) {
        val payload = "$idNumber;$qrContents".toByteArray()
        client.publish(resultTopic, payload, 1, false)
    }

}