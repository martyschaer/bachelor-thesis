package dev.schaer.vc.verifier

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import dev.schaer.mrzreader.model.DocumentData
import dev.schaer.mrzreader.utils.ErrorDialog
import dev.schaer.vc.crypto.CryptoUtils
import dev.schaer.vc.model.certificate.VaccinationCertificate
import dev.schaer.vc.serialization.Serializer
import dev.schaer.vc.verifier.databinding.FragmentCertscanresultBinding
import java.lang.StringBuilder
import java.security.PublicKey

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class VerificationResultFragment : Fragment() {

    private var _binding: FragmentCertscanresultBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentCertscanresultBinding.inflate(inflater, container, false)

        parentFragment?.view?.setOnKeyListener { _: View, keyCode: Int, _: KeyEvent ->
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                findNavController().navigate(R.id.action_VerificationResultFragment_to_InitialFragment)
            }
            false
        }

        _binding?.buttonScanNextCertificate?.setOnClickListener {
            val bundle = Bundle()
            bundle.putBoolean(SCAN_IMMEDIATE, true)
            findNavController().navigate(
                R.id.action_VerificationResultFragment_to_InitialFragment,
                bundle
            )
        }

        val certScanResult =
            arguments?.getString(CERT_SCAN_RESULT)?.let(this::extractVaccinationCertificate)
        val idDocument = arguments?.getParcelable<DocumentData>(ID_SCAN_RESULT)

        if (certScanResult != null && idDocument != null) {
            onScan(certScanResult, idDocument)
        } else if (certScanResult == null) {
            onError("Something went wrong scanning the QR Code.")
        } else if (idDocument == null) {
            onError("Something went wrong scanning the ID.")
        }

        return binding.root

    }


    private fun onScan(certScanResult: VaccinationCertificate, idDocument: DocumentData) {
        val bytesToHex =
            CryptoUtils.instance().bytesToHex(Serializer.instance().serialize(certScanResult.data))
        Log.v("VRF", bytesToHex)
        val trustChainValid = isTrustChainValid(certScanResult)
        val idMatches = isCertificateMatchingId(certScanResult, idDocument)
        val dataValid = certScanResult.data.isValid

        val signaturesValid = if(TrustRoots.isKnown(certScanResult.trustRootIdentifier)) {
            CryptoUtils.instance().verifyDetail(certScanResult, TrustRoots.get(certScanResult.trustRootIdentifier)).valid()
        } else {
            false
        }

        val sb = StringBuilder()
        if(trustChainValid && idMatches && dataValid) {
            sb.append("✔️ OK ✔️\n")
        } else {
            sb.append("❌ NOT OK ❌\n")
        }

        sb.append("trustChain  = $trustChainValid\n")
        if(!trustChainValid) {
            sb.append("\troot known    = ${TrustRoots.isKnown(certScanResult.trustRootIdentifier)}\n")
            sb.append("\tissuer OK     = ${IssuerRevocationList.isNotRevoked(certScanResult.issuerKey)}\n")
            sb.append("\tsignatures OK = $signaturesValid\n")
        }

        sb.append("id matches  = $idMatches\n")
        if(!idMatches) {
            sb.append("\tscanned id  = ${idDocument.documentNumber}\n")
            sb.append("\tcert. id    = ${certScanResult.data.patient.identifier.value}\n")
        }

        sb.append("rules valid = $dataValid\n")

        binding.textviewCertificateResult.text = sb.toString()
    }

    private fun isCertificateMatchingId(
        certificate: VaccinationCertificate,
        idDocument: DocumentData
    ): Boolean {
        return certificate.data.patient.identifier.value == idDocument.documentNumber
    }

    private fun isTrustChainValid(certificate: VaccinationCertificate): Boolean {
        if(TrustRoots.isKnown(certificate.trustRootIdentifier)) {
            val trustRoot = TrustRoots.get(certificate.trustRootIdentifier)
            val signaturesValid =  CryptoUtils.instance().verifyDetail(certificate, trustRoot).valid()
            return signaturesValid && IssuerRevocationList.isNotRevoked(certificate.issuerKey)
        }
        return false
    }

    private fun onError(msg: String) {
        ErrorDialog.newInstance(msg).show(childFragmentManager, "dialog")
    }

    private fun extractVaccinationCertificate(qrContent: String): VaccinationCertificate {
        return Serializer.instance().deserializeFromQR(qrContent.trim())
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}