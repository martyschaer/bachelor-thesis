package dev.schaer.vc.verifier

import android.content.Context
import dev.schaer.vc.crypto.CryptoUtils
import java.security.PublicKey

class TrustRoots {
    companion object {

        private val map = mutableMapOf<String, PublicKey>()
        private var initialized = false

        fun init(context: Context) {
            map.putAll(loadTrustRootKeys(context))
            initialized = true
        }

        fun isKnown(identifier: String): Boolean {
            if(initialized) {
                return identifier in map
            }
            error("Trustroots not initialized")
        }

        fun all(): List<PublicKey> {
            return map.values.toList()
        }

        fun get(identifier: String): PublicKey {
            if(initialized) {
                return map[identifier]!!
            }
            error("Trustroots not initialized")
        }

        private fun loadTrustRootKeys(context: Context): Map<String, PublicKey> {
            return listOf("dev.schaer.testroot.pub", "dev.schaer.arctic.admin.pub")
                .map { loadTrustRootKey(context, it) }
                .associateBy { key -> CryptoUtils.instance().byte2b64str(CryptoUtils.instance().hash(key.encoded)) }
        }

        private fun loadTrustRootKey(context: Context, name: String): PublicKey {
            val stream = context.assets.open(name)
            val size = stream.available()
            val buffer = ByteArray(size)
            stream.read(buffer)
            stream.close()
            return CryptoUtils.instance().parsePublicKey(buffer)
        }
    }
}