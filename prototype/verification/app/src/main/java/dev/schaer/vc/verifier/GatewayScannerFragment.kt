package dev.schaer.vc.verifier

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.journeyapps.barcodescanner.ScanContract
import com.journeyapps.barcodescanner.ScanOptions
import dev.schaer.mrzreader.api.DocumentScanContract
import dev.schaer.mrzreader.model.DocumentData
import dev.schaer.vc.model.certificate.VaccinationCertificate.MAGIC_BYTES_AS_STRING
import dev.schaer.vc.verifier.databinding.FragmentInitialBinding

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class GatewayScannerFragment : Fragment() {

    private var _binding: FragmentInitialBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private var _certScanResult: String? = null
    private var _idScanResult: String? = null

    private val scanDocumentLauncher =
        registerForActivityResult(DocumentScanContract()) { doc: DocumentData? ->
            if (doc != null && _certScanResult != null) {
                _idScanResult = doc.documentNumber
                onResult(_idScanResult!!, _certScanResult!!)
            }
        }

    private val barcodeLauncher = registerForActivityResult(ScanContract()) { result ->
        if (result.contents != null && result.contents.startsWith(MAGIC_BYTES_AS_STRING)) {
            _certScanResult = result.contents
            GatewayChannel.INSTANCE.showId()
            scanDocumentLauncher.launch(true)
        } else if (result.contents != null) {
            // unable to find vaccCERT QR-Code, retry
            scanQR()
        } else {
            // cancelled
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentInitialBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        nextScan()
    }

    private fun nextScan() {
        _idScanResult = null
        _certScanResult = null
        scanQR()
    }

    private fun onResult(idScanResult: String, certScanResult: String) {
        GatewayChannel.INSTANCE.sendScanResult(certScanResult, idScanResult)
        Handler(Looper.getMainLooper()).postDelayed({ nextScan() }, 3000)
    }

    private fun scanQR() {
        GatewayChannel.INSTANCE.showQR()
        val options = ScanOptions()
        options.setBeepEnabled(true)
        options.setDesiredBarcodeFormats(ScanOptions.QR_CODE)
        options.setPrompt("")
        options.captureActivity = CertificateCaptureActivity::class.java
        barcodeLauncher.launch(options)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}