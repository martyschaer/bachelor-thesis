package dev.schaer.vc.verifier

import android.content.Context
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import dev.schaer.vc.crypto.CryptoException
import dev.schaer.vc.crypto.CryptoUtils
import java.security.AccessControlContext
import java.security.PublicKey

class IssuerRevocationList {
    companion object {
        private val revoked = mutableSetOf<String>()

        fun init(context: Context) {
            val queue = Volley.newRequestQueue(context)
            val url = "https://arctic.schaer.dev/irl.txt"
            val stringRequest = StringRequest(
                Request.Method.GET, url,
                Response.Listener { response ->
                    val entries = response.lines()
                    val loaded = entries.map { addEntry(it) }.count { it }
                    Toast.makeText(context, "Reloaded $loaded revoked issuers", Toast.LENGTH_LONG).show()
                },
                Response.ErrorListener { error ->
                    Toast.makeText(context, "err: " + error.message, Toast.LENGTH_LONG).show()
                })

            queue.add(stringRequest)
        }

        fun isRevoked(issuer: PublicKey): Boolean {
            val issuerId =
                CryptoUtils.instance().byte2b64str(CryptoUtils.instance().hash(issuer.encoded))
            return issuerId in revoked
        }

        fun isNotRevoked(issuer: PublicKey): Boolean {
            return !isRevoked(issuer)
        }

        fun addEntry(entry: String): Boolean {
            if(entry.isBlank()) {
                return false
            }
            val (revocation, signature) = entry.split(";")
            if (isSignedByKnownTrustRoot(revocation, signature)) {
                revoked.add(revocation)
                return true
            }
            return false
        }

        private fun isSignedByKnownTrustRoot(revocation: String, signature: String): Boolean {
            for (pubKey in TrustRoots.all()) {
                try {
                    if (CryptoUtils.instance()
                            .verify(revocation.toByteArray(), CryptoUtils.instance().b64str2byte(signature), pubKey)
                    ) {
                        return true
                    }
                } catch (e: CryptoException) {
                    continue
                }
            }
            return false
        }

        fun clear() {
            revoked.clear()
        }
    }
}