# Prototype
There are five modules to the prototype for this thesis:

- `issuance` - issuing certificates
  * Desktop App using JavaFX
- `verification` - verifying the validity of certificates
  * Android App
- `model` - a shared library containing the models and verification logic
- `access` - automated access to certificate restricted areas
  * Java and Python
- `id_checkdigit_calculator` - a utility for generating MRZs
