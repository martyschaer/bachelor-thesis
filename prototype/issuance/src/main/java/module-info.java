module dev.schaer.vc.issuance {
    requires javafx.controls;
    requires model;
    requires javafx.fxml;
    requires mapdb;
    requires dev.dirs;
    requires core;
    requires java.desktop;
    requires org.jsoup;
    requires openhtmltopdf.core;
    requires openhtmltopdf.pdfbox;
    opens dev.schaer.vc.issuance to javafx.graphics;
    opens dev.schaer.vc.issuance.controller to javafx.fxml;
}