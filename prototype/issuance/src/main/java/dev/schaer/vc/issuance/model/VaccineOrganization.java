package dev.schaer.vc.issuance.model;

import dev.schaer.vc.model.fhir.Organization;
import dev.schaer.vc.model.fhir.Vaccine;
import dev.schaer.vc.model.fhir.presets.Selection;
import javafx.util.Pair;

import java.util.List;
import java.util.Map;

public class VaccineOrganization {
    public static final Map<String, Pair<Selection<Vaccine>, Selection<Organization>>> VACC_ORG_ASSOC = Map.of(
            Vaccine.MODERNA.getLabel(), new Pair<>(Vaccine.MODERNA, Organization.MODERNA)
            , Vaccine.BIONTECH.getLabel() , new Pair<>(Vaccine.BIONTECH, Organization.BIONTECH)
            , Vaccine.JANSSEN.getLabel(), new Pair<>(Vaccine.JANSSEN, Organization.JANSSEN)
            , Vaccine.ASTRAZENECA.getLabel(), new Pair<>(Vaccine.ASTRAZENECA, Organization.ASTRAZENECA)
    );
}
