package dev.schaer.vc.issuance.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

public class FileUtils {
    public static String loadResource(String name) throws IOException {
        try(var is = FileUtils.class.getClassLoader().getResourceAsStream(name)) {
            if(is == null) {
                throw new IllegalStateException("Unable to load resource " + name);
            }
            try (var isr = new InputStreamReader(is); var r = new BufferedReader(isr)) {
                return r.lines().collect(Collectors.joining(System.lineSeparator()));
            }
        }
    }
}
