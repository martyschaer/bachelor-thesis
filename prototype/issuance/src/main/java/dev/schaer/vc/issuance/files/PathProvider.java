package dev.schaer.vc.issuance.files;

import dev.dirs.ProjectDirectories;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public final class PathProvider {
    private static final String MAP_DB_NAME = "keys.db";

    private static PathProvider INSTANCE;

    private final ProjectDirectories projDirs;

    private PathProvider() {
        projDirs = ProjectDirectories.from("dev", "schaer", "vc-issuance");
        try {
            Files.createDirectories(Paths.get(projDirs.dataDir));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static PathProvider instance() {
        if(INSTANCE == null) {
            INSTANCE = new PathProvider();
        }
        return INSTANCE;
    }

    public Path getKeysDbPath() {
        return Paths.get(projDirs.dataDir, MAP_DB_NAME);
    }

}
