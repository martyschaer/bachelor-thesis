package dev.schaer.vc.issuance.controller;

import dev.schaer.vc.crypto.CryptoUtils;
import dev.schaer.vc.crypto.IssuerPackage;
import dev.schaer.vc.issuance.model.KeyManager;
import dev.schaer.vc.issuance.model.VaccinationCertificateManager;
import dev.schaer.vc.issuance.model.VaccineOrganization;
import dev.schaer.vc.model.certificate.VaccinationCertificate;
import dev.schaer.vc.model.fhir.*;
import dev.schaer.vc.model.fhir.presets.Selection;
import dev.schaer.vc.serialization.Serializer;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.util.Pair;

import java.net.URL;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

public class DataEntryController implements Initializable {

    @FXML
    public GridPane dataEntryContainer;

    @FXML
    public ComboBox<Pair<Selection<Vaccine>, Selection<Organization>>> vaccineSelector;

    @FXML
    public TextField lotNumber;

    @FXML
    public DatePicker vaccinationDate;

    @FXML
    public ComboBox<Selection<Protocol>> protocolSelector;

    @FXML
    public TextField idDocNumber;

    @FXML
    public Button issueCertificate;

    private final KeyManager keyManager = KeyManager.instance();
    private final Serializer serializer = Serializer.instance();
    private final CryptoUtils cryptoUtils = CryptoUtils.instance();
    private final VaccinationCertificateManager certManager = VaccinationCertificateManager.instance();

    private Optional<IssuerPackage> activePackage = Optional.empty();

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        setDisable(true);
        clear();

        initVaccineSelector();
        initVaccinationDatePicker();
        initProtocolSelector();
        keyManager.addActivePackageListener(this::setIssuerPackage);

        initValidation();
    }

    private void clear() {
        vaccineSelector.getSelectionModel().clearSelection();
        lotNumber.clear();
        vaccinationDate.setValue(LocalDate.now());
        protocolSelector.getSelectionModel().clearSelection();
        idDocNumber.clear();
        runValidation();
    }

    public void setIssuerPackage(IssuerPackage issuerPackage) {
        setDisable(issuerPackage == null);
        activePackage = Optional.ofNullable(issuerPackage);
    }

    public void setDisable(final boolean disable) {
        dataEntryContainer.setDisable(disable);
    }

    private void initVaccineSelector() {
        vaccineSelector.setConverter(new VaccineOrganizationAssocationConverter());
        vaccineSelector.getItems().addAll(VaccineOrganization.VACC_ORG_ASSOC.values().stream() //
                .sorted(Comparator.comparing(p -> p.getKey().getLabel())) //
                .collect(Collectors.toList()));
    }

    private void initVaccinationDatePicker() {
        vaccinationDate.setValue(LocalDate.now());
        vaccinationDate.setConverter(new DateConverter());
        vaccinationDate.setDayCellFactory(picker -> new DateCell() {
            @Override
            public void updateItem(LocalDate date, boolean empty) {
                super.updateItem(date, empty);
                if(date.isAfter(LocalDate.now())) {
                    setDisable(true);
                    setStyle("-fx-background-color: #ffc0cb;");
                    setTooltip(new Tooltip("You cannot select dates in the future"));
                }
            }
        });
        vaccinationDate.getEditor().setDisable(true);
        vaccinationDate.getEditor().setOpacity(1);
    }

    private void initProtocolSelector() {
        protocolSelector.setConverter(new ProtocolConverter());
        protocolSelector.getItems().addAll(Protocol.AVAILABLE);
    }

    private void initValidation() {
        vaccineSelector.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> runValidation());
        lotNumber.textProperty().addListener((observable, oldValue, newValue) -> runValidation());
        vaccinationDate.valueProperty().addListener((observable, oldValue, newValue) -> runValidation());
        protocolSelector.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> runValidation());
        idDocNumber.textProperty().addListener((observable, oldValue, newValue) -> runValidation());
    }

    private void runValidation() {
        boolean vaccineSelectorValid = vaccineSelector.getSelectionModel().getSelectedItem() != null;
        boolean lotNumberValid = lotNumber.getText() != null && !lotNumber.getText().isBlank();
        boolean vaccinationDateValid = vaccinationDate.getValue() != null;
        boolean protocolSelectorValid = protocolSelector.getSelectionModel().getSelectedItem() != null;
        boolean idDocNumberFieldValid = idDocNumber.getText() != null && !idDocNumber.getText().isBlank();

        boolean valid = vaccineSelectorValid
                && lotNumberValid
                && vaccinationDateValid
                && protocolSelectorValid
                && idDocNumberFieldValid;

        issueCertificate.setDisable(!valid);
    }

    public void issueCertificate(ActionEvent actionEvent) {
        activePackage.ifPresent(issuerPackage -> {
            Immunization immunization = createImmunizationRecord();
            certManager.event(createCertificate(immunization, issuerPackage));
            clear();
        });
    }

    private Immunization createImmunizationRecord() {
        ImmunizationBuilder builder = new ImmunizationBuilder();
        var selectedVaccine = vaccineSelector.getSelectionModel().getSelectedItem();
        builder.vaccine(selectedVaccine.getKey().getValue());
        builder.manufacturer(selectedVaccine.getValue().getValue());
        builder.lotNumber(lotNumber.getText());
        builder.occurence(vaccinationDate.getValue());
        builder.recorded(LocalDate.now());
        builder.protocol(protocolSelector.getSelectionModel().getSelectedItem().getValue());
        builder.patient(idDocNumber.getText());
        return builder.build();
    }

    private VaccinationCertificate createCertificate(Immunization data, IssuerPackage activePackage) {
        try {
            VaccinationCertificate.Builder builder = new VaccinationCertificate.Builder();

            byte[] dataBytes = serializer.serialize(data);
            System.out.println(CryptoUtils.instance().bytesToHex(dataBytes));
            builder.data(data);
            builder.dataSignature(cryptoUtils.sign(dataBytes, activePackage.getPrivKey()));
            builder.issuerKey(activePackage.getPubKey());
            builder.issuerKeySignature(activePackage.getPubKeySignature());
            builder.trustRootIdentifier(cryptoUtils.byte2b64str(activePackage.getTrustRootId()));
            return builder.build();
        }catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
}
