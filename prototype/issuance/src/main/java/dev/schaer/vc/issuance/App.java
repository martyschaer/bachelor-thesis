package dev.schaer.vc.issuance;

import dev.schaer.vc.crypto.IssuerPackage;
import dev.schaer.vc.issuance.controller.DataEntryController;
import dev.schaer.vc.issuance.controller.HandoutController;
import dev.schaer.vc.issuance.controller.IssuanceController;
import dev.schaer.vc.issuance.controller.KeyManagementController;
import dev.schaer.vc.issuance.model.KeyManager;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import dev.schaer.vc.crypto.CryptoUtils;

import java.security.KeyPair;

public class App extends Application {

    @Override
    public void start(Stage stage) throws Exception{
        FXMLLoader loader = new FXMLLoader(App.class.getResource("/issuance.fxml"));
        BorderPane pane = loader.load();
        var scene = new Scene(pane, 1000, 480);
        stage.setScene(scene);

        IssuanceController controller = loader.getController();

        FXMLLoader keyManagementLoader = new FXMLLoader(App.class.getResource("/keyManagement.fxml"));
        StackPane keyManagementPane = keyManagementLoader.load();
        KeyManagementController keyManagementController = keyManagementLoader.getController();
        controller.setKeyManagementController(keyManagementController);

        FXMLLoader dataEntryLoader = new FXMLLoader(App.class.getResource("/dataEntry.fxml"));
        dataEntryLoader.load();
        DataEntryController dataEntryController = dataEntryLoader.getController();
        controller.setDataEntryController(dataEntryController);

        FXMLLoader handoutLoader = new FXMLLoader(App.class.getResource("/handout.fxml"));
        handoutLoader.load();

        stage.show();

        stage.setOnCloseRequest(event -> {
            KeyManager.instance().close();
            Platform.exit();
            System.exit(0);
        });
    }

    public static void main(String[] args) {
        launch();
    }

}