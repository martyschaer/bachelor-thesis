package dev.schaer.vc.issuance.controller;

import dev.schaer.vc.issuance.model.VaccinationCertificateManager;
import dev.schaer.vc.issuance.utils.ExceptionHandler;
import dev.schaer.vc.issuance.utils.PdfUtils;
import dev.schaer.vc.issuance.utils.QRUtils;
import dev.schaer.vc.model.certificate.VaccinationCertificate;
import dev.schaer.vc.serialization.Serializer;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.UUID;

public class HandoutController implements Initializable {
    private final VaccinationCertificateManager certManager = VaccinationCertificateManager.instance();
    private final Serializer serializer = Serializer.instance();
    private Optional<VaccinationCertificate> certificate = Optional.empty();
    private Optional<Image> certificateQrCode = Optional.empty();

    @FXML
    public ImageView qrDisplay;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        certManager.addListener(this::onCertificate);
        qrDisplay.setDisable(true);
    }

    private void onCertificate(VaccinationCertificate cert) {
        certificate = Optional.ofNullable(cert);
        certificate.ifPresent(c -> {
            try {
                Image qrCode = QRUtils.createQRCode(serializer.serializeForQR(c));
                certificateQrCode = Optional.of(qrCode);
                qrDisplay.setImage(qrCode);
                qrDisplay.setDisable(false);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }

    public void exportPdf(ActionEvent actionEvent) {
        if (certificate.isPresent() && certificateQrCode.isPresent()) {
            var cert = certificate.get();
            var img = certificateQrCode.get();
            var imgName = UUID.randomUUID().toString();
            writeImageToFile(Paths.get("/tmp", imgName + ".png").toFile(), img);
            File file = chooseFile(String.format("certificate_%s.pdf",
                    cert.getData().getPatient().getIdentifier().getValue().toLowerCase()), actionEvent);
            try {
                PdfUtils.writePdf(file, imgName, cert);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void exportPng(ActionEvent actionEvent) {
        if (certificate.isPresent() && certificateQrCode.isPresent()) {
            var cert = certificate.get();
            var img = certificateQrCode.get();
            File file = chooseFile(String.format("certificate_%s.png",
                    cert.getData().getPatient().getIdentifier().getValue().toLowerCase()), actionEvent);
            if(file != null) {
                writeImageToFile(file, img);
            }
        }
    }

    private File chooseFile(String name, ActionEvent e) {
        FileChooser fc = new FileChooser();
        fc.setInitialFileName(name);
        return fc.showSaveDialog(((Button) e.getSource()).getScene().getWindow());
    }

    private void writeImageToFile(File f, Image img) {
        int width = (int) img.getWidth();
        int height = (int) img.getHeight();
        var reader = img.getPixelReader();

        BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_4BYTE_ABGR);
        for(int y = 0; y < height;y++) {
            for(int x = 0; x < height;x++) {
                bi.setRGB(x, y, reader.getArgb(x, y));
            }
        }

        try {
            ImageIO.write(bi, "png", f);
        } catch (IOException e) {
            ExceptionHandler.instance().handle(e);
        }
    }
}
