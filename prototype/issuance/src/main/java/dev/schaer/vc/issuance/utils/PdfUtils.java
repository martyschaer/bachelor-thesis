package dev.schaer.vc.issuance.utils;

import com.openhtmltopdf.css.parser.property.PageSize;
import com.openhtmltopdf.pdfboxout.PdfRendererBuilder;
import dev.schaer.vc.model.certificate.VaccinationCertificate;
import org.jsoup.Jsoup;
import org.jsoup.helper.W3CDom;
import org.w3c.dom.Document;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Paths;
import java.time.format.DateTimeFormatter;

public class PdfUtils {
    public static void writePdf(File location, String imgName, VaccinationCertificate certificate) throws IOException {
        if(location == null || imgName == null || certificate == null) {
            return;
        }

        var doc = prepareHtml(imgName, certificate);
        try(var out = new FileOutputStream(location)) {
            var builder = new PdfRendererBuilder();
            builder.withUri(location.toURI().toString());
            builder.toStream(out);
            builder.withW3cDocument(doc, Paths.get("/tmp").toUri().toString());
            builder.run();
        }

    }

    private static Document prepareHtml(String imgName, VaccinationCertificate certificate) throws IOException {
        org.jsoup.nodes.Document doc;
        var immunization = certificate.getData();
        var html = FileUtils.loadResource("pdf.html");
        html = html.replace("{{var.imgname}}", imgName);
        html = html.replace("{{var.id}}", immunization.getPatient().getIdentifier().getValue());
        html = html.replace("{{var.vacc}}", immunization.getVaccineCode().toString());
        html = html.replace("{{var.date}}", immunization.getOccurenceDateTime().format(DateTimeFormatter.ISO_DATE));
        html = html.replace("{{var.issuedate}}", immunization.getRecorded().format(DateTimeFormatter.ISO_DATE));
        html = html.replace("{{var.doses}}", immunization.getProtocolApplied().toString());
        html = html.replace("{{var.lot}}", immunization.getLotNumber());

        doc = Jsoup.parse(html);
        return new W3CDom().fromJsoup(doc);
    }
}
