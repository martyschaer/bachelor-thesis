package dev.schaer.vc.issuance.controller;

import dev.schaer.vc.model.fhir.Protocol;
import dev.schaer.vc.model.fhir.presets.Selection;
import javafx.util.StringConverter;

public class ProtocolConverter extends StringConverter<Selection<Protocol>> {
    @Override
    public String toString(Selection<Protocol> object) {
        return object.getLabel();
    }

    @Override
    public Selection<Protocol> fromString(String string) {
        return Protocol.AVAILABLE.stream() //
                .filter(p -> p.getLabel().equals(string)) //
                .findFirst() //
                .orElse(null);
    }
}
