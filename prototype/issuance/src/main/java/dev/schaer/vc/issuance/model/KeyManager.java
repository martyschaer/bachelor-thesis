package dev.schaer.vc.issuance.model;

import dev.schaer.vc.crypto.IssuerPackage;
import dev.schaer.vc.issuance.files.PathProvider;
import org.mapdb.DB;
import org.mapdb.DBMaker;
import org.mapdb.Serializer;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Consumer;

public class KeyManager {
    private static KeyManager INSTANCE;
    private static final String PACKAGES = "packages";

    private final ConcurrentMap<String, byte[]> packages;
    private final DB db;
    private Optional<IssuerPackage> activePackage;
    private List<Consumer<IssuerPackage>> activePackageListeners = new ArrayList<>();

    private KeyManager() {
        db = DBMaker //
                .fileDB(PathProvider.instance().getKeysDbPath().toFile()) //
                .fileMmapEnable() //
                .make();
        packages = db.hashMap(PACKAGES, Serializer.STRING, Serializer.BYTE_ARRAY) //
                .createOrOpen();

    }

    public void close() {
        db.close();
    }

    public static KeyManager instance() {
        if(INSTANCE == null) {
            INSTANCE = new KeyManager();
        }
        return INSTANCE;
    }

    public byte[] get(String key) {
        return packages.get(key);
    }

    public void put(String key, byte[] content) {
        packages.put(key, content);
        db.commit();
    }

    public Set<String> getKeys() {
        return packages.keySet();
    }

    public void setActivePackage(IssuerPackage activePackage) {
        this.activePackage = Optional.ofNullable(activePackage);
        activePackageListeners.forEach(it -> it.accept(activePackage));
    }

    public void addActivePackageListener(Consumer<IssuerPackage> listener) {
        activePackageListeners.add(listener);
    }

}
