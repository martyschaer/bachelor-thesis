package dev.schaer.vc.issuance.utils;

import javafx.scene.control.Alert;

public class ExceptionHandler {
    private static ExceptionHandler INSTANCE;

    private ExceptionHandler() {

    }

    public static ExceptionHandler instance() {
        if(INSTANCE == null) {
            INSTANCE = new ExceptionHandler();
        }
        return INSTANCE;
    }

    public void handle(Exception e) {
        Alert a = new Alert(Alert.AlertType.ERROR);
        a.setHeaderText(e.getClass().getSimpleName());
        a.setContentText(e.getMessage());
        a.showAndWait();
    }
}
