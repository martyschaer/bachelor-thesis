package dev.schaer.vc.issuance.utils;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import dev.schaer.vc.crypto.CryptoUtils;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;

import java.util.Map;

public class QRUtils {
    private static final int DIMENSIONS = 300;
    private static final Map<EncodeHintType, Object> OPTIONS = Map.of( //
            EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L
    );

    public static Image createQRCode(String content) throws WriterException {
        QRCodeWriter writer = new QRCodeWriter();
        BitMatrix matrix = writer.encode(content, BarcodeFormat.QR_CODE, DIMENSIONS, DIMENSIONS, OPTIONS);
        return bitMatrixToImage(matrix);
    }

    private static Image bitMatrixToImage(BitMatrix matrix) {
        Canvas canvas = new Canvas(DIMENSIONS, DIMENSIONS);
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.setFill(Color.WHITE);
        gc.fillRect(0, 0, DIMENSIONS, DIMENSIONS);

        gc.setFill(Color.BLACK);
        for(int y = 0; y < DIMENSIONS; y++) {
            for(int x = 0; x < DIMENSIONS; x++) {
                if(matrix.get(x, y)) {
                    gc.fillRect(x, y, 1, 1);
                }
            }
        }

        return canvas.snapshot(null, null);
    }
}
