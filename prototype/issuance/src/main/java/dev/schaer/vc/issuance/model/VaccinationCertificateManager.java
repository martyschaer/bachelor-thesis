package dev.schaer.vc.issuance.model;

import dev.schaer.vc.model.certificate.VaccinationCertificate;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public final class VaccinationCertificateManager {
    private static VaccinationCertificateManager INSTANCE;

    private final List<Consumer<VaccinationCertificate>> listeners = new ArrayList<>();

    private VaccinationCertificateManager() {

    }

    public static VaccinationCertificateManager instance() {
        if(INSTANCE == null) {
            INSTANCE = new VaccinationCertificateManager();
        }
        return INSTANCE;
    }

    public void addListener(Consumer<VaccinationCertificate> listener) {
        this.listeners.add(listener);
    }

    public void event(VaccinationCertificate certificate) {
        this.listeners.forEach(it -> it.accept(certificate));
    }
}
