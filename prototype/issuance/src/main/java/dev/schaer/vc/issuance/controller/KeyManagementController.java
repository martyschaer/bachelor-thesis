package dev.schaer.vc.issuance.controller;

import dev.schaer.vc.crypto.CryptoException;
import dev.schaer.vc.crypto.IssuerPackage;
import dev.schaer.vc.issuance.model.KeyManager;
import dev.schaer.vc.serialization.Serializer;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.stage.FileChooser;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.util.Optional;
import java.util.ResourceBundle;

public class KeyManagementController implements javafx.fxml.Initializable {
    private static final String ISSUER_PACKAGE_EXTENSION = ".package.bin";

    @FXML
    private StackPane stackPane;

    @FXML
    private ListView<String> listView;

    @FXML
    private Label activePackageLabel;

    private final ObjectProperty<String> selectedPackageName = new SimpleObjectProperty<>();
    private final ObjectProperty<IssuerPackage> activePackage = new SimpleObjectProperty<>();

    private final KeyManager keyManager = KeyManager.instance();
    private final Serializer serializer = Serializer.instance();

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        listView.setOnMouseClicked((click) -> {
            if (click.getClickCount() == 1 && listView.getSelectionModel().getSelectedItem() != null) {
                String selectedItem = listView.getSelectionModel().getSelectedItem();
                selectedPackageName.setValue(selectedItem);
            }
        });
        listView.getItems().addAll(keyManager.getKeys());
    }

    public void addPackage(ActionEvent actionEvent) throws IOException {
        FileChooser fc = new FileChooser();
        fc.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Issuer Packages", "*" + ISSUER_PACKAGE_EXTENSION));
        File file = fc.showOpenDialog(((Button) actionEvent.getSource()).getScene().getWindow());
        if (file != null) {
            String packageName = file.getName().split(ISSUER_PACKAGE_EXTENSION)[0];
            keyManager.put(packageName, Files.readAllBytes(file.toPath()));
            listView.getItems().add(packageName);
        }
    }

    public void activatePackage(ActionEvent actionEvent) {
        if (selectedPackageName.getValue() != null) {
            String packageName = selectedPackageName.getValue();
            Dialog<String> pwPrompt = createPasswordDialog(packageName);
            Optional<String> result = pwPrompt.showAndWait();
            result.ifPresent((password -> decryptAndSetActive(packageName, password)));
        }
    }

    private void decryptAndSetActive(String packageName, String password) {
        byte[] encryptedPackage = keyManager.get(packageName);
        do {
            try {
                IssuerPackage decryptedPackage = serializer.deserialize(encryptedPackage, password);
                activePackage.setValue(decryptedPackage);
                keyManager.setActivePackage(decryptedPackage);
                activePackageLabel.setText("active: " + packageName);
                return;
            } catch (CryptoException e) {
                Optional<String> result = createPasswordDialog(packageName).showAndWait();
                if (result.isPresent()) {
                    password = result.get();
                } else {
                    return; // user pressed cancel, abort retry
                }
            }
        } while (true);

    }

    private Dialog<String> createPasswordDialog(String packageName) {
        Dialog<String> dialog = new Dialog<>();
        dialog.setTitle("Unlock " + packageName);
        dialog.getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);

        PasswordField pwField = new PasswordField();
        HBox container = new HBox();
        container.setAlignment(Pos.CENTER_LEFT);
        container.setSpacing(5);
        container.getChildren().addAll(new Label("Password for Package:"), pwField);
        dialog.getDialogPane().setContent(container);
        dialog.setResultConverter((dialogButton -> {
            if (dialogButton == ButtonType.OK) {
                return pwField.getText();
            }
            return null;
        }));
        return dialog;
    }
}
