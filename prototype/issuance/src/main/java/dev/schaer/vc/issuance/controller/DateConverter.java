package dev.schaer.vc.issuance.controller;

import javafx.util.StringConverter;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DateConverter extends StringConverter<LocalDate> {
    private static final DateTimeFormatter FMT = DateTimeFormatter.ISO_LOCAL_DATE;

    @Override
    public String toString(LocalDate date) {
        return date.format(FMT);
    }

    @Override
    public LocalDate fromString(String string) {
        return LocalDate.parse(string, FMT);
    }
}
