package dev.schaer.vc.issuance.controller;

import dev.schaer.vc.crypto.IssuerPackage;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.Initializable;

import java.net.URL;
import java.util.ResourceBundle;

public class IssuanceController implements Initializable {

    private KeyManagementController keyManagementController;
    private DataEntryController dataEntryController;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }

    public void setKeyManagementController(KeyManagementController keyManagementController) {
        this.keyManagementController = keyManagementController;
    }

    public void setDataEntryController(DataEntryController dataEntryController) {
        this.dataEntryController = dataEntryController;
    }
}
