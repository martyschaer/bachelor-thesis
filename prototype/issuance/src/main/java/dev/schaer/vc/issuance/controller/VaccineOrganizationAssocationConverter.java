package dev.schaer.vc.issuance.controller;

import dev.schaer.vc.issuance.model.VaccineOrganization;
import dev.schaer.vc.model.fhir.Organization;
import dev.schaer.vc.model.fhir.Vaccine;
import dev.schaer.vc.model.fhir.presets.Selection;
import javafx.util.Pair;
import javafx.util.StringConverter;

public class VaccineOrganizationAssocationConverter extends StringConverter<Pair<Selection<Vaccine>, Selection<Organization>>> {
    @Override
    public String toString(Pair<Selection<Vaccine>, Selection<Organization>> object) {
        if(object == null) {
            return "null";
        }
        return object.getKey().getLabel();
    }

    @Override
    public Pair<Selection<Vaccine>, Selection<Organization>> fromString(String label) {
        return VaccineOrganization.VACC_ORG_ASSOC.get(label);
    }
}
