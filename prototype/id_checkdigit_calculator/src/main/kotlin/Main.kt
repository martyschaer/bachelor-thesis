fun main() {
    val type = "ID"
    val issuer = "CHE"
    val docNum = "D1337"
    val dateOfBirth = "060916"
    val sex = "M"
    val expiry = "301106"
    val nationality = "CHE"
    val surname = "WILHELM"
    val givenName = "TELL"

    printMrz(type, issuer, docNum, dateOfBirth, sex, expiry, nationality, surname, givenName)
}

fun printMrz(
    type: String,
    issuer: String,
    docNum: String,
    dateOfBirth: String,
    sex: String,
    expiry: String,
    nationality: String,
    surname: String,
    givenName: String
) {
    val sb = StringBuilder()
    sb.append(type.padEnd(2, '<'))
    sb.append(issuer)
    sb.append(docNum.padEnd(9, '<'))
    sb.append(checksum(docNum))
    sb.append("<".repeat(15))
    sb.append('\n')

    sb.append(dateOfBirth)
    sb.append(checksum(dateOfBirth))
    sb.append(sex.padEnd(1, '<'))
    sb.append(expiry)
    sb.append(checksum(expiry))
    sb.append(nationality)
    sb.append("<".repeat(11))
    sb.append(
        checksum(
            "${docNum.padEnd(9, '<')}${checksum(docNum)}"
                    + "$dateOfBirth${checksum(dateOfBirth)}"
        +"$expiry${checksum(expiry)}"
        )
    )
    sb.append('\n')

    val nameStr = "$surname<<$givenName"
    sb.append(nameStr.padEnd(30, '<'))

    println(sb.toString())
}

fun checksum(input: String): Int {
    var checkSum = 0
    var weightIdx = 0
    val weighting = intArrayOf(7, 3, 1)
    input.chars().map { c ->
        when (c) {
            60 -> 0
            in (48..57) -> c - 48
            in (65..90) -> c - 55
            else -> error("'$c, ${c.toChar()}' not allowed")
        }
    }
        .forEach {
            checkSum = (checkSum + it * weighting[weightIdx % 3]) % 10
            weightIdx++
        }
    return checkSum % 10
}
