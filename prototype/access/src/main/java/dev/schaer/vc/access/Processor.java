package dev.schaer.vc.access;

import dev.schaer.vc.crypto.CryptoException;
import dev.schaer.vc.crypto.CryptoUtils;
import dev.schaer.vc.crypto.VerificationResult;
import dev.schaer.vc.model.certificate.VaccinationCertificate;
import dev.schaer.vc.serialization.Serializer;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.security.PublicKey;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Stream;

public class Processor {
    private static final Path ISSUER_REVOCATION_LIST_PATH = Paths.get("/", "var", "cache", "ARCTIC-processor.irl.txt");
    private static final Path LED_CONTROL_PATH = Paths.get("/", "tmp", "led_control.bin");
    private final MqttConnectOptions options;
    private final String qrTopic;
    private final String idTopic;
    private final String resultTopic;
    private final MqttClient client;
    private Map<String, PublicKey> trustRootKeys;
    private Set<String> revoked;

    public Processor(Properties properties) throws MqttException {
        options = new MqttConnectOptions();
        options.setAutomaticReconnect(true);
        options.setCleanSession(true);
        options.setUserName(properties.getProperty("mqtt.user"));
        options.setPassword(properties.getProperty("mqtt.pass").toCharArray());
        final String serverUri = properties.getProperty("mqtt.host");
        this.client = new MqttClient(serverUri, UUID.randomUUID().toString(), new MemoryPersistence());
        this.qrTopic = properties.getProperty("mqtt.topic.qr");
        this.idTopic = properties.getProperty("mqtt.topic.id");
        this.resultTopic = properties.getProperty("mqtt.topic.result");
    }

    public void run() throws MqttException, IOException {
        setup();
        client.subscribe(this.qrTopic, (__, message) -> processQr(message));
        client.subscribe(this.idTopic, (__, message) -> processId(message));
        client.subscribe(this.resultTopic, (__, message) -> processResult(message));
    }

    private void processId(MqttMessage message) {
        System.out.println("set ID light");
        setLEDs(false, false, true, false);
    }

    private void processQr(MqttMessage message) {
        System.out.println("set QR light");
        setLEDs(false, false, false, true);
    }

    private void processResult(MqttMessage message) {
        System.out.println("Process result message");
        final String[] tokens = new String(message.getPayload()).split(";");
        try {
            final String idDocumentNumber = tokens[0];
            final String qrContents = tokens[1];

            processResult(idDocumentNumber, qrContents);
        } catch (Exception e) {
            System.err.println("Failed to process result. Received '" + new String(message.getPayload()) + "'");
            playErrorAnimation();
        }
    }

    private void processResult(String idDocumentNumber, String qrContents) {
        VaccinationCertificate certificate = Serializer.instance().deserializeFromQR(qrContents);

        Optional<PublicKey> trustRootKey = getMatchingRootKey(certificate.getTrustRootIdentifier());

        boolean isValid = true;

        if (trustRootKey.isEmpty()) {
            System.err.println("Unknown trust root: " + certificate.getTrustRootIdentifier());
            isValid &= false;
        }

        String issuerId = CryptoUtils.instance().byte2b64str(CryptoUtils.instance().hash(certificate.getIssuerKey().getEncoded()));

        if (revoked.contains(issuerId)) {
            System.err.println("Revoked issuer: " + certificate.getTrustRootIdentifier());
            isValid &= false;
        }

        Optional<VerificationResult> result = trustRootKey.map(key -> CryptoUtils.instance().verifyDetail(certificate, key));

        isValid &= result.map(VerificationResult::valid).orElse(false);
        isValid &= Objects.equals(idDocumentNumber, certificate.getData().getPatient().getIdentifier().getValue());
        isValid &= certificate.getData().isValid();

        if (isValid) {
            System.out.println("OK");
            setLEDs(false, true, false, false);
        } else {
            setLEDs(true, false, false, false);
            System.err.printf("ID: cert=%s, scan=%s%n", certificate.getData().getPatient().getIdentifier().getValue(), idDocumentNumber);
            System.err.println(certificate.getData());
        }
    }

    private Optional<PublicKey> getMatchingRootKey(String keyId) {
        return Optional.ofNullable(this.trustRootKeys.get(keyId));
    }

    public void cleanup() {
        try {
            this.client.disconnect();
            this.client.close();
        } catch (Exception e) {
            throw new IllegalStateException("Error during cleanup", e);
        }
    }

    private void setup() throws MqttException, IOException {
        this.client.connect(options);

        this.revoked = loadIRL();

        this.trustRootKeys = new ConcurrentHashMap<>();

        for (String name : List.of("dev.schaer.testroot.pub", "dev.schaer.arctic.admin.pub")) {
            PublicKey key = loadTrustRootKey(name);
            String id = CryptoUtils.instance().byte2b64str(CryptoUtils.instance().hash(key.getEncoded()));
            this.trustRootKeys.put(id, key);
        }

        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                try {
                    HttpRequest request = HttpRequest.newBuilder()
                            .uri(new URI("https://arctic.schaer.dev/irl.txt"))
                            .GET()
                            .build();
                    HttpResponse<Stream<String>> response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofLines());
                    onReloadIRL(response.body());
                } catch (URISyntaxException | IOException | InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, 10_000L, 10_000L);
    }

    private void onReloadIRL(Stream<String> results) throws IOException {
        this.revoked.clear();
        results.filter(it -> !it.isBlank()).forEach(line -> {
            String[] tokens = line.split(";");
            String revoke = tokens[0];
            String signature = tokens[1];
            if (isSignedByAnyTrustRoot(revoke, signature)) {
                this.revoked.add(revoke);
                System.out.printf("'%s' revoked", revoke);
            }
        });
        System.out.printf("%d entries on the issuer revocation list", this.revoked.size());
        byte[] irlBytes = String.join("\n", this.revoked).getBytes(StandardCharsets.UTF_8);
        Files.write(ISSUER_REVOCATION_LIST_PATH, irlBytes, StandardOpenOption.CREATE, StandardOpenOption.WRITE);
    }

    private boolean isSignedByAnyTrustRoot(String data, String signature) {
        for (PublicKey key : this.trustRootKeys.values()) {
            byte[] dataBytes = data.getBytes(StandardCharsets.UTF_8);
            byte[] sigBytes = CryptoUtils.instance().b64str2byte(signature);
            try {
                if (CryptoUtils.instance().verify(dataBytes, sigBytes, key)) {
                    return true;
                }
            } catch (CryptoException e) {
                continue;
            }
        }
        return false;
    }

    private Set<String> loadIRL() throws IOException {
        if (!Files.exists(ISSUER_REVOCATION_LIST_PATH)) {
            return new HashSet<>();
        }
        return new HashSet<>(Files.readAllLines(ISSUER_REVOCATION_LIST_PATH));
    }

    private PublicKey loadTrustRootKey(String name) throws IOException {
        InputStream keyStream = Processor.class.getClassLoader().getResourceAsStream(name);
        byte[] keyBytes = Objects.requireNonNull(keyStream).readAllBytes();
        return CryptoUtils.instance().parsePublicKey(keyBytes);
    }

    private void playErrorAnimation() {
        try {
            setLEDs(true, false, false, false);
            Thread.sleep(200);
            setLEDs(false, true, false, false);
            Thread.sleep(200);
            setLEDs(false, false, true, false);
            Thread.sleep(200);
            setLEDs(false, false, false, true);
            Thread.sleep(200);
            setLEDs(false, false, true, false);
            Thread.sleep(200);
            setLEDs(false, true, false, false);
            Thread.sleep(200);
            setLEDs(true, false, false, false);
            Thread.sleep(200);
            setLEDs(true, true, true, true);
            Thread.sleep(1000);
            setLEDs(false, false, false, false);
        } catch (InterruptedException e) {
            // DO NOTHING
        }
    }

    private void setLEDs(boolean er, boolean ok, boolean id, boolean qr) {
        String result //
                = (er ? "1" : "0")
                + (ok ? "1" : "0")
                + (id ? "1" : "0")
                + (qr ? "1" : "0");
        try {
            Files.write(LED_CONTROL_PATH, result.getBytes(), StandardOpenOption.CREATE, StandardOpenOption.WRITE);
        } catch (IOException e) {
            throw new IllegalStateException("Unable to control LEDs", e);
        }
    }
}
