package dev.schaer.vc.access;
import java.util.Properties;

public class Main {

    public static void main(String[] args) throws Exception{
        Properties p = new Properties();
        p.load(Main.class.getResourceAsStream("/application.properties"));
        new Processor(p).run();
    }
}
