#!/usr/bin/python
from gpiozero import LED
from time import sleep
from os.path import exists

is_fail = LED(26)
is_success = LED(19)
show_id = LED(13)
show_qr = LED(6)

leds = [is_fail, is_success, show_id, show_qr]

def follow(thefile):
	while True:
		thefile.seek(0, 0)
		line = thefile.readline()
		if not line:
			sleep(0.1)
			continue
		yield line

def set_leds(control_string):
	print("setting leds to ", control_string)
	idx = 0
	for c in control_string:
		if c == '1':
			leds[idx].on()
		else:
			leds[idx].off()
		idx += 1

if __name__ == '__main__':
	ctrl_file_name = "/tmp/led_control.bin"
	if not exists(ctrl_file_name):
		open(ctrl_file_name, 'w').close()

	control_file = open(ctrl_file_name, "r")
	prevline = '0000'
	control = follow(control_file)
	for line in control:
		if line == prevline:
			continue
		prevline = line
		set_leds(line.strip())
