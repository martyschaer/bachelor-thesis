package dev.schaer.vc.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.cbor.CBORFactory;
import dev.schaer.vc.crypto.CryptoUtils;
import dev.schaer.vc.model.fhir.*;
import dev.schaer.vc.serialization.CompressionUtils;
import dev.schaer.vc.serialization.Serializer;
import dev.schaer.vc.serialization.cbor.VaccinationCertificateModule;
import org.junit.jupiter.api.Test;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.KeyPair;
import java.time.LocalDate;
import java.util.List;

import static dev.schaer.vc.model.TestHelper.buildImmunization;
import static dev.schaer.vc.model.TestHelper.bytesToHex;
import static org.junit.jupiter.api.Assertions.*;

public class ImmunizationTest {
    private static final List<TargetDisease> C19 = List.of(TargetDisease.COVID_19);
    private static final String LOT_NUM = "3001234";
    private static final Patient PATIENT = new Patient(new Identifier("A0132456"));
    private static final LocalDate TODAY = LocalDate.now();
    private static final LocalDate LAST_WEEK = TODAY.minusDays(7);
    private static final LocalDate NEXT_WEEK = TODAY.plusDays(7);
    private static final LocalDate TWO_YEARS_AGO = TODAY.minusYears(2);
    private static final Protocol PROTOCOL_1_2 = new Protocol(1, 2, C19);
    private static final Protocol PROTOCOL_2_2 = new Protocol(2, 2, C19);

    @Test
    public void test() throws Exception {
        Immunization expected = buildImmunization();

        ObjectMapper mapper = new ObjectMapper(new CBORFactory());
        mapper.registerModule(new VaccinationCertificateModule());

        byte[] bytes = mapper.writeValueAsBytes(expected);

        Immunization result = mapper.readValue(bytes, Immunization.class);
        assertEquals(expected, result);
    }

    @Test
    public void testIsValid_shouldBeValid() { var immunization = new Immunization();
        immunization.setLotNumber(LOT_NUM);
        immunization.setManufacturer(Organization.MODERNA.getValue());
        immunization.setPatient(PATIENT);
        immunization.setRecorded(TODAY);
        immunization.setOccurenceDateTime(TODAY);
        immunization.setVaccineCode(Vaccine.MODERNA.getValue());
        immunization.setProtocolApplied(PROTOCOL_2_2);
        assertTrue(immunization.isValid());
    }

    @Test
    public void testIsValid_shouldBeInvalid_whenNotEnoughDoses() {
        var immunization = new Immunization();
        immunization.setLotNumber(LOT_NUM);
        immunization.setManufacturer(Organization.MODERNA.getValue());
        immunization.setPatient(PATIENT);
        immunization.setRecorded(TODAY);
        immunization.setOccurenceDateTime(TODAY);
        immunization.setVaccineCode(Vaccine.MODERNA.getValue());
        immunization.setProtocolApplied(PROTOCOL_1_2);
        assertFalse(immunization.isValid());
    }

    @Test
    public void testIsValid_shouldBeInvalid_whenUnApprovedVaccine() {
        var immunization = new Immunization();
        immunization.setLotNumber(LOT_NUM);
        immunization.setManufacturer(Organization.ASTRAZENECA.getValue());
        immunization.setPatient(PATIENT);
        immunization.setRecorded(TODAY);
        immunization.setOccurenceDateTime(TODAY);
        immunization.setVaccineCode(Vaccine.ASTRAZENECA.getValue());
        immunization.setProtocolApplied(PROTOCOL_2_2);
        assertFalse(immunization.isValid());
    }

    @Test
    public void testIsValid_shouldBeInvalid_whenWrongDates_1() {
        var immunization = new Immunization();
        immunization.setLotNumber(LOT_NUM);
        immunization.setManufacturer(Organization.MODERNA.getValue());
        immunization.setPatient(PATIENT);
        immunization.setRecorded(LAST_WEEK);
        immunization.setOccurenceDateTime(TODAY);
        immunization.setVaccineCode(Vaccine.MODERNA.getValue());
        immunization.setProtocolApplied(PROTOCOL_2_2);
        assertFalse(immunization.isValid());
    }

    @Test
    public void testIsValid_shouldBeInvalid_whenWrongDates_2() {
        var immunization = new Immunization();
        immunization.setLotNumber(LOT_NUM);
        immunization.setManufacturer(Organization.MODERNA.getValue());
        immunization.setPatient(PATIENT);
        immunization.setRecorded(TWO_YEARS_AGO);
        immunization.setOccurenceDateTime(TWO_YEARS_AGO);
        immunization.setVaccineCode(Vaccine.MODERNA.getValue());
        immunization.setProtocolApplied(PROTOCOL_2_2);
        assertFalse(immunization.isValid());
    }

}
