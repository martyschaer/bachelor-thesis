package dev.schaer.vc.model;

import dev.schaer.vc.model.fhir.*;

import javax.swing.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class TestHelper {
    public static Immunization buildImmunization() {
        return new Immunization(//
                buildVaccine(), //
                new Patient(new Identifier("S00004156")), //
                LocalDate.now(), //
                LocalDate.now(), //
                new Organization(new Identifier("ORG-100031184"), "Moderna Biotech Spain S.L."), //
                "3002541", //
                buildProtocol() //
        );
    }

    public static long getFuzzingOptions() {
        return 1L //
                * Vaccine.AVAILABLE.size() //
                * Organization.AVAILABLE.size() //
                * Protocol.AVAILABLE.size() //
                * generatePatients().size() //
                * generateDates().size() //
                * generateDates().size() //
                * generateLotNumbers().size();
    }

    public static void runFuzzing(Consumer<Immunization> test) {
        long cases = getFuzzingOptions();
        long count = 0;
        System.out.printf("Running fuzzing (%d cases)%n",cases);
        for(var vaccine : Vaccine.AVAILABLE) {
            for(var org : Organization.AVAILABLE) {
                for(var protocol : Protocol.AVAILABLE) {
                    for(var patient : generatePatients()) {
                        for(var occurence: generateDates()) {
                            for(var recorded: generateDates()) {
                                for(var lotNumber: generateLotNumbers()) {
                                    count++;
                                    if(count % 500 == 0) {
                                        System.out.printf("%d/%d (%.3f)%n", count, cases, ((double)count)/cases);
                                    }
                                    test.accept(new Immunization( //
                                            vaccine.getValue(), //
                                            patient, //
                                            recorded, //
                                            occurence, //
                                            org.getValue(), //
                                            lotNumber, //
                                            protocol.getValue() //
                                    ));
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private static List<String> generateLotNumbers() {
        return List.of("\n", "", "A", "a", "01", "1", "S01", "S1", "0041234986096");
    }

    private static List<LocalDate> generateDates() {
        List<LocalDate> dates = new ArrayList<>();
        for(int offset = -5; offset <= 5; offset++) {
            dates.add(LocalDate.now().plusDays(offset));
        }
        return dates;
    }

    private static List<Patient> generatePatients() {
        return Set.of(
                "", "S", "S1", "S01", "1", "01", "S0809873434"
        ).stream() //
                .map(Identifier::new) //
                .map(Patient::new) //
                .collect(Collectors.toList());
    }

    public static Vaccine buildVaccine() {
        return new Vaccine(
                List.of(new Code("https://ec.europa.eu/health/documents/community-register/html/", "EU/1/20/1507")),
                "Spikevax (previously COVID-19 Vaccine Moderna)"
        );
    }

    public static Protocol buildProtocol() {
        return new Protocol(
                2, //
                2, //
                List.of(new TargetDisease(List.of(new Code("http://snomed.info/sct", "840539006")), "COVID-19"))
        );
    }

    public static String bytesToHex(byte[] bytes) {
        StringBuilder builder = new StringBuilder(bytes.length * 2);
        for(byte b : bytes) {
            builder.append(String.format("%02X ", b));
        }
        return builder.toString();
    }
}
