package dev.schaer.vc.model;

import dev.schaer.vc.crypto.CryptoUtils;
import dev.schaer.vc.model.certificate.VaccinationCertificate;
import dev.schaer.vc.model.fhir.Immunization;
import dev.schaer.vc.serialization.Serializer;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.security.KeyPair;
import java.util.Objects;

@Disabled
public class FuzzingTest {
    private KeyPair trustRootKeyPair = CryptoUtils.instance().generate();
    private KeyPair issuerKeyPair = CryptoUtils.instance().generate();
    private byte[] issuerKeySignature = CryptoUtils.instance().sign(issuerKeyPair.getPublic().getEncoded(), trustRootKeyPair.getPrivate());
    private byte[] trustRootIdentifier = CryptoUtils.instance().hash(trustRootKeyPair.getPublic().getEncoded());
    private String trustRootIdentifierStr = CryptoUtils.instance().byte2b64str(trustRootIdentifier);

    @Test
    public void test() throws Exception{
        TestHelper.runFuzzing(this::assertImmunizationSigning);
    }

    private boolean assertImmunizationSigning(Immunization immunization) {
        var builder = new VaccinationCertificate.Builder();
        builder.data(immunization);
        builder.dataSignature(CryptoUtils.instance().sign(Serializer.instance().serialize(immunization), issuerKeyPair.getPrivate()));
        builder.issuerKey(issuerKeyPair.getPublic());
        builder.issuerKeySignature(issuerKeySignature);
        builder.trustRootIdentifier(trustRootIdentifierStr);
        var cert = builder.build();

        String qrContent = Serializer.instance().serializeForQR(cert);

        var result = Serializer.instance().deserializeFromQR(qrContent);
        if(!(Objects.equals(cert, result) && CryptoUtils.instance().verify(result, trustRootKeyPair.getPublic()))) {
            System.out.println(immunization);
            return true;
        }
        return false;
    }
}
