package dev.schaer.vc.crypto;

import dev.schaer.vc.serialization.Serializer;
import org.junit.jupiter.api.Test;

import java.security.KeyPair;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class IssuerPackageTest {
    private static final String PASSWORD = "NotAG00dP45sW0rD!";

    @Test
    public void testSerializingIssuerPackage() throws Exception {
        var expected = createIssuerPackage();
        byte[] serialized = Serializer.instance().serialize(expected, PASSWORD);
        var actual = Serializer.instance().deserialize(serialized, PASSWORD);
        assertEquals(expected, actual);
    }

    @Test
    public void testUnableToDecryptUsingWrongPassword() throws Exception {
        var issuerPackage = createIssuerPackage();
        byte[] serialized = Serializer.instance().serialize(issuerPackage, PASSWORD);
        CryptoException exception = assertThrows(CryptoException.class, () -> {
            Serializer.instance().deserialize(serialized, "wrong");
        });
        assertEquals("Unable to decrypt data", exception.getMessage());
    }

    private IssuerPackage createIssuerPackage() {
        KeyPair issuerKeyPair = CryptoUtils.instance().generate();
        KeyPair trustRootKeyPair = CryptoUtils.instance().generate();

        byte[] issuerKeySignature = CryptoUtils.instance().sign(issuerKeyPair.getPublic().getEncoded(), trustRootKeyPair.getPrivate());
        byte[] trustRootId = CryptoUtils.instance().hash(trustRootKeyPair.getPublic().getEncoded());

        return new IssuerPackage( //
                issuerKeyPair.getPublic(), //
                issuerKeyPair.getPrivate(), //
                issuerKeySignature, //
                trustRootId
        );
    }
}
