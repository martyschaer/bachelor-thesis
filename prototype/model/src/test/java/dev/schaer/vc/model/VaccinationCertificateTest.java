package dev.schaer.vc.model;

import dev.schaer.vc.crypto.CryptoUtils;
import dev.schaer.vc.model.certificate.VaccinationCertificate;
import dev.schaer.vc.model.fhir.Immunization;
import dev.schaer.vc.serialization.Serializer;
import org.junit.jupiter.api.Test;

import java.security.KeyPair;
import java.util.Arrays;

import static dev.schaer.vc.model.TestHelper.buildImmunization;
import static dev.schaer.vc.model.TestHelper.bytesToHex;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class VaccinationCertificateTest {

    @Test
    public void test() throws Exception {
        Immunization immunization = buildImmunization();

        final KeyPair trustRootKeypair = CryptoUtils.instance().generate();
        final KeyPair issuerKeyPair = CryptoUtils.instance().generate();
        final byte[] issuerKeySignature = CryptoUtils.instance().sign(issuerKeyPair.getPublic().getEncoded(), trustRootKeypair.getPrivate());
        final byte[] trustRootIdentifier = CryptoUtils.instance().hash(trustRootKeypair.getPublic().getEncoded());
        final String trustRootIdentifierStr = CryptoUtils.instance().byte2b64str(trustRootIdentifier);

        final byte[] trustRootPubKey = trustRootKeypair.getPublic().getEncoded();

        byte[] dataBytes = Serializer.instance().serialize(immunization);

        System.out.println(bytesToHex(dataBytes));

        final byte[] dataSignature = CryptoUtils.instance().sign(dataBytes, issuerKeyPair.getPrivate());

        assertTrue(CryptoUtils.instance().verify(dataBytes, dataSignature, issuerKeyPair.getPublic()));

        VaccinationCertificate.Builder builder = new VaccinationCertificate.Builder();
        builder.data(immunization);
        builder.dataSignature(dataSignature);
        builder.issuerKey(issuerKeyPair.getPublic());
        builder.issuerKeySignature(issuerKeySignature);
        builder.trustRootIdentifier(trustRootIdentifierStr);
        VaccinationCertificate expected = builder.build();

        String qrCodeContents = Serializer.instance().serializeForQR(expected);

        VaccinationCertificate result = Serializer.instance().deserializeFromQR(qrCodeContents);
        assertTrue(CryptoUtils.instance().verify(result, CryptoUtils.instance().parsePublicKey(trustRootPubKey)));
        assertEquals(expected, result);
    }

}
