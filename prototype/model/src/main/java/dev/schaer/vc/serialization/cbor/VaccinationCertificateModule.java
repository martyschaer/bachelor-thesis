package dev.schaer.vc.serialization.cbor;

import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.module.SimpleDeserializers;
import com.fasterxml.jackson.databind.module.SimpleSerializers;

import java.security.PrivateKey;
import java.security.PublicKey;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;

public class VaccinationCertificateModule extends Module {

    @Override
    public String getModuleName() {
        return "VaccinationCertificateModule";
    }

    @Override
    public Version version() {
        return new Version(0, 0, 1, "SNAPSHOT", "devschaer.vc", "serialization");
    }

    @Override
    public void setupModule(SetupContext setupContext) {
        SimpleSerializers serializers = new SimpleSerializers(List.of( //
                new DateSerializer(), //
                new PublicKeySerializer(), //
                new PrivateKeySerializer()
        ));
        setupContext.addSerializers(serializers);

        SimpleDeserializers deserializers = new SimpleDeserializers(Map.of( //
                LocalDate.class, new DateDeserializer(), //
                PublicKey.class, new PublicKeyDeserializer(), //
                PrivateKey.class, new PrivateKeyDeserializer()
        ));
        setupContext.addDeserializers(deserializers);
    }
}
