package dev.schaer.vc.crypto;

public class CryptoException extends RuntimeException{
    public CryptoException(String message, Throwable cause) {
        super(message, cause);
    }
}
