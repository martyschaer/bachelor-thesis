package dev.schaer.vc.serialization.cbor;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DateSerializer extends JsonSerializer<LocalDate> {

    public static final DateTimeFormatter FMT = DateTimeFormatter.ISO_DATE;

    @Override
    public void serialize(LocalDate value, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeString(value.format(FMT));
    }

    @Override
    public Class<LocalDate> handledType() {
        return LocalDate.class;
    }
}
