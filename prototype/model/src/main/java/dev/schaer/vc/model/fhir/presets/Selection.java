package dev.schaer.vc.model.fhir.presets;

public class Selection<T> {
    private final String label;
    private final T value;

    public Selection(String label, T value) {

        this.label = label;
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Selection<?> selection = (Selection<?>) o;

        if (label != null ? !label.equals(selection.label) : selection.label != null) return false;
        return value != null ? value.equals(selection.value) : selection.value == null;
    }

    @Override
    public int hashCode() {
        int result = label != null ? label.hashCode() : 0;
        result = 31 * result + (value != null ? value.hashCode() : 0);
        return result;
    }

    public String getLabel() {
        return label;
    }

    public T getValue() {
        return value;
    }
}
