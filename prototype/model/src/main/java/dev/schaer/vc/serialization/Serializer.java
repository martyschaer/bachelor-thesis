package dev.schaer.vc.serialization;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.cbor.CBORFactory;
import dev.schaer.vc.crypto.CryptoUtils;
import dev.schaer.vc.crypto.IssuerPackage;
import dev.schaer.vc.exception.SerializationException;
import dev.schaer.vc.model.certificate.VaccinationCertificate;
import dev.schaer.vc.model.fhir.Immunization;
import dev.schaer.vc.serialization.cbor.VaccinationCertificateModule;

import java.io.IOException;

import static dev.schaer.vc.model.certificate.VaccinationCertificate.MAGIC_BYTES_AS_STRING;

public class Serializer {
    private final ObjectMapper mapper;
    private static Serializer INSTANCE;

    private Serializer() {
        mapper = new ObjectMapper(new CBORFactory());
        mapper.registerModule(new VaccinationCertificateModule());
    }

    public static Serializer instance() {
        if (INSTANCE == null) {
            INSTANCE = new Serializer();
        }
        return INSTANCE;
    }

    public String bytesToStr(byte[] bytes) {
        return MAGIC_BYTES_AS_STRING + CryptoUtils.instance().byte2b64str(bytes);
    }

    public byte[] strToBytes(String str) {
        return CryptoUtils.instance().b64str2byte(str.substring(MAGIC_BYTES_AS_STRING.length()));
    }

    public String serializeForQR(VaccinationCertificate certificate) {
        byte[] rawBytes = serialize(certificate);
        return bytesToStr(CompressionUtils.deflate(rawBytes));
    }

    public VaccinationCertificate deserializeFromQR(String str) {
        return deserialize(CompressionUtils.inflate(strToBytes(str)));
    }

    public byte[] serialize(VaccinationCertificate certificate) {
        try {
            return mapper.writeValueAsBytes(certificate);
        } catch (JsonProcessingException e) {
            throw new SerializationException("Unable to serialize certificate", e);
        }
    }

    public byte[] serialize(Immunization immunization) {
        try {
            return mapper.writeValueAsBytes(immunization);
        } catch (JsonProcessingException e) {
            throw new SerializationException("Unable to serialize immunization record", e);
        }
    }

    public VaccinationCertificate deserialize(byte[] bytes) {
        try {
            return mapper.readValue(bytes, VaccinationCertificate.class);
        } catch (IOException e) {
            throw new SerializationException("Unable to deserialize certificate", e);
        }
    }

    public byte[] serialize(IssuerPackage issuerPackage, String password) {
        byte[] bytes;
        try {
            bytes = mapper.writeValueAsBytes(issuerPackage);
        } catch (JsonProcessingException e) {
            throw new SerializationException("Unable to serialize issuerPackage", e);
        }
        return CryptoUtils.instance().encrypt(bytes, password);
    }

    public IssuerPackage deserialize(byte[] bytes, String password) {
        byte[] decrypted = CryptoUtils.instance().decrypt(bytes, password);
        try {
            return mapper.readValue(decrypted, IssuerPackage.class);
        } catch (IOException e) {
            throw new SerializationException("Unable to deserialize issuerPackage", e);
        }
    }
}
