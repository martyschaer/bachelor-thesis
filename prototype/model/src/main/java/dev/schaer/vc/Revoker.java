package dev.schaer.vc;

import dev.schaer.vc.crypto.CryptoUtils;
import dev.schaer.vc.crypto.IssuerPackage;
import dev.schaer.vc.serialization.Serializer;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.PrivateKey;

public class Revoker {
    public static void main(String[] args) throws Exception {
        Path packageToRevoke = Paths.get("kara.package.bin");
        Path rootKeyPath = Paths.get("dev.schaer.arctic.admin.key");
        String packagePassword = "password";

        new Revoker().run(packageToRevoke, rootKeyPath, packagePassword);
    }

    private void run(Path packageToRevokePath, Path rootKeyPath, String packagePassword) throws Exception{
        PrivateKey rootKey = CryptoUtils.instance().parsePrivateKey(Files.readAllBytes(rootKeyPath));
        byte[] packageBytes = Files.readAllBytes(packageToRevokePath);
        IssuerPackage packageToRevoke = Serializer.instance().deserialize(packageBytes, packagePassword);
        String revokedId = CryptoUtils.instance().hash2b64str(packageToRevoke.getPubKey().getEncoded());
        byte[] revocationSignature = CryptoUtils.instance().sign(revokedId.getBytes(StandardCharsets.UTF_8), rootKey);
        System.out.printf("%s;%s%n", revokedId, CryptoUtils.instance().byte2b64str(revocationSignature));
    }
}
