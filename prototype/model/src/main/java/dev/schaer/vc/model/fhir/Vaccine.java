package dev.schaer.vc.model.fhir;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import dev.schaer.vc.model.fhir.presets.Selection;

import java.util.List;
import java.util.Set;

@JsonPropertyOrder({"text", "coding"})
public class Vaccine{
    private static final String SYSTEM = "https://ec.europa.eu/health/documents/community-register/html/";
    public static final Selection<Vaccine> MODERNA = new Selection<>("Spikevax (Moderna)", new Vaccine(List.of(new Code(SYSTEM, "EU/1/20/1507")), "Spikevax"));
    public static final Selection<Vaccine> BIONTECH = new Selection<>("Comirnaty (BioNTech)", new Vaccine(List.of(new Code(SYSTEM, "EU/1/20/1528")), "Comirnaty"));
    public static final Selection<Vaccine> JANSSEN = new Selection<>("COVID-19 Vaccine Janssen", new Vaccine(List.of(new Code(SYSTEM, "EU/1/20/1525")), "COVID-19 Vaccine Janssen"));
    public static final Selection<Vaccine> ASTRAZENECA = new Selection<>("Vaxzevria (AstraZeneca)", new Vaccine(List.of(new Code(SYSTEM, "EU/1/20/1529")), "Vaxzevria"));

    public static final List<Selection<Vaccine>> AVAILABLE = List.of(MODERNA, BIONTECH, JANSSEN, ASTRAZENECA);
    public static final Set<Vaccine> ACCEPTED = Set.of(MODERNA.getValue(), BIONTECH.getValue(), JANSSEN.getValue());

    private List<Code> coding;
    private String text;

    public Vaccine() {
        // default for Jackson
    }

    public Vaccine(List<Code> coding, String text) {
        this.coding = coding;
        this.text = text;
    }

    @Override
    public String toString() {
        return "Vaccine(" + text + ')';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Vaccine vaccine = (Vaccine) o;

        if (coding != null ? !coding.equals(vaccine.coding) : vaccine.coding != null) return false;
        return text != null ? text.equals(vaccine.text) : vaccine.text == null;
    }

    @Override
    public int hashCode() {
        int result = coding != null ? coding.hashCode() : 0;
        result = 31 * result + (text != null ? text.hashCode() : 0);
        return result;
    }

    public List<Code> getCoding() {
        return coding;
    }

    public void setCoding(List<Code> coding) {
        this.coding = coding;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
