package dev.schaer.vc;

import dev.schaer.vc.model.certificate.VaccinationCertificate;
import dev.schaer.vc.serialization.Serializer;

public class ManualDecoder {
    public static void main(String[] args) {
        final String qrContents = "vaccCERTeNplkltoFEkUhsfENRIQFhZBccFERRZx7EtmklGJEjNjMuSmiZcx4rKVqtM9pd1dbVX1mNasklVcVsQ38fLgBYwI3kXwhvSj7opLdFeJPuiDDwZB1BcVVNpKlODl6ZxTdU6d/3x1IkKQRJElJJKBoJi5vgMSiO0jScGT0XpKlKEWBR5BCTkBWFnD1M26OC5ywIwTIOtVbCZ1I6nXUYfJ9sDtAQ41qXRtzYYSwph60MgIRERCr6SNzKXcQzK0MCPUs/sjohxwcis1QzN1zUibGUuEQoLbu6gopS8WaBrgeRBw5iNltCIgRxY1wnDgKnFCU7LdwKMyTHKwqarkWlG6jhbHjou8wEJYBlwNQGTog9PBbeTRzWpA5tmECt9BYe+0JSqSgItVbWMVSlxVk9vT/D0Dt6OzKWnoul6jm0ZateEgWMAxrBhpkB9V86kB8zmTDDOnwfcdCiRyJeI2yCwVgAT0R19ToJmUnq6Zr+u1nxGURggoAMJjLpB51LOYJrCMR1kWGztW5bNJY34cECbgE/hlTFBJS5D3ZFlJAKcgsupSfHkeb2QYKyQehiySsIK6MPKJRtIwk6YZUypEALwFwsJafY3+04SKOTub/60vGzdh4qhTPq6ifEli/J6/gmjwl5Nbc8lK7+DVhqGWu7x6/Lr/89funB5KvNt3PdHd9Wy43zr/vs/7oXXV4Vv3bh3aUf33wlP7H05+cjvcMtkd2b0uaqtlUFoKzfrSsurE0bo5d8ozHxam8PNHQ+eqZm7PPfhx8Nqinysa/zzdunvbFZVy5pU+OOuyOH7u1wt7+wq09nH7xT4+6e7A67Y/Lj29MZDhY/K/ffz23me/VbY7NzP49xnDlWj64Qx9MjV3dvfbF/7GA7n701TKmxNN/22YfWNXUNh59cKUYwPDRwqLyaxN/7ysfLE18bi+SkgeCNnJmMyPbUXvXFbXbeeWt6Uhneps727h+a7W1ZtCWmu3QjbMptyGzaBZmVJPbjWujz8CHSpm3g==";
        System.out.println("Deserializing " + qrContents);
        VaccinationCertificate certificate = Serializer.instance().deserializeFromQR(qrContents);
        System.out.println(certificate.getData());
        System.out.println(certificate.getTrustRootIdentifier());
    }
}
