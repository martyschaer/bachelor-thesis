package dev.schaer.vc.crypto;

public class VerificationResult {
    private final boolean isPublicKeyKnown;
    private final boolean isIssuerSignatureValid;
    private final boolean isDataSignatureValid;


    public VerificationResult(boolean isPublicKeyKnown, boolean isIssuerSignatureValid, boolean isDataSignatureValid) {
        this.isPublicKeyKnown = isPublicKeyKnown;
        this.isIssuerSignatureValid = isIssuerSignatureValid;
        this.isDataSignatureValid = isDataSignatureValid;
    }

    public boolean valid() {
        return isPublicKeyKnown && isIssuerSignatureValid && isDataSignatureValid;
    }

    @Override
    public String toString() {
        if(valid()) {
            return "OK";
        }
        return String.format("NOK(root=%b, issuerSig=%b, dataSig=%b)",
                isPublicKeyKnown, isIssuerSignatureValid, isDataSignatureValid);
    }

    public static class Builder {
        private boolean isPublicKeyKnown;
        private boolean isIssuerSignatureValid;
        private boolean isDataSignatureValid;

        public Builder publicKeyKnown(boolean isPublicKeyKnown) {
            this.isPublicKeyKnown = isPublicKeyKnown;
            return this;
        }

        public Builder issuerSignatureValid(boolean isIssuerSignatureValid) {
            this.isIssuerSignatureValid = isIssuerSignatureValid;
            return this;
        }

        public Builder dataSignatureValid(boolean isDataSignatureValid) {
            this.isDataSignatureValid = isDataSignatureValid;
            return this;
        }

        public VerificationResult build() {
            return new VerificationResult(isPublicKeyKnown, isIssuerSignatureValid, isDataSignatureValid);
        }
    }
}
