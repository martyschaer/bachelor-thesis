package dev.schaer.vc.serialization.cbor;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import dev.schaer.vc.crypto.CryptoUtils;

import java.io.IOException;
import java.security.Key;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.time.LocalDate;

public class PublicKeyDeserializer extends JsonDeserializer<PublicKey> {

    @Override
    public PublicKey deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        return CryptoUtils.instance().parsePublicKey(jsonParser.getBinaryValue());
    }

    @Override
    public Class<PublicKey> handledType() {
        return PublicKey.class;
    }
}
