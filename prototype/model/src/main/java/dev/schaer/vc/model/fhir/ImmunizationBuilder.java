package dev.schaer.vc.model.fhir;

import java.time.LocalDate;

public class ImmunizationBuilder {
    private Vaccine vaccineCode;
    private Patient patient;
    private LocalDate recorded;
    private LocalDate occurenceDateTime;
    private Organization manufacturer;
    private String lotNumber;
    private Protocol protocolApplied;

    public ImmunizationBuilder vaccine(Vaccine vaccine) {
        this.vaccineCode = vaccine;
        return this;
    }

    public ImmunizationBuilder patient(String patientId) {
        this.patient = new Patient(new Identifier(patientId));
        return this;
    }

    public ImmunizationBuilder patient(Patient patient) {
        this.patient = patient;
        return this;
    }

    public ImmunizationBuilder recorded(LocalDate recorded) {
        this.recorded = recorded;
        return this;
    }

    public ImmunizationBuilder occurence(LocalDate occurence) {
        this.occurenceDateTime = occurence;
        return this;
    }

    public ImmunizationBuilder manufacturer(Organization manufacturer) {
        this.manufacturer = manufacturer;
        return this;
    }

    public ImmunizationBuilder lotNumber(String lotNumber) {
        this.lotNumber = lotNumber;
        return this;
    }

    public ImmunizationBuilder protocol(Protocol protocol)  {
        this.protocolApplied = protocol;
        return this;
    }

    public Immunization build() {
        return new Immunization(
                vaccineCode,
                patient,
                recorded,
                occurenceDateTime,
                manufacturer,
                lotNumber,
                protocolApplied
        );
    }
}
