package dev.schaer.vc.model.fhir;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({"type", "identifier"})
public class Patient {
    private final String type = "Patient";
    private Identifier identifier;

    public Patient() {
        // default constructor for Jackson
    }

    public Patient(Identifier identifier) {
        this.identifier = identifier;
    }

    public Identifier getIdentifier() {
        return identifier;
    }

    public void setIdentifier(Identifier identifier) {
        this.identifier = identifier;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Patient patient = (Patient) o;

        if (type != null ? !type.equals(patient.type) : patient.type != null) return false;
        return identifier != null ? identifier.equals(patient.identifier) : patient.identifier == null;
    }

    @Override
    public int hashCode() {
        int result = type != null ? type.hashCode() : 0;
        result = 31 * result + (identifier != null ? identifier.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Patient{" +
                "id=" + identifier +
                '}';
    }
}
