package dev.schaer.vc;

import dev.schaer.vc.crypto.CryptoUtils;
import dev.schaer.vc.crypto.IssuerPackage;
import dev.schaer.vc.serialization.Serializer;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;

public class IssuerPackageGenerator {
    public static void main(String[] args) throws Exception{
        String rootName = "dev.schaer.arctic.admin";
        Path out = Paths.get("kara" + ".package.bin");

        new IssuerPackageGenerator().run(out, rootName);
    }

    private void run(Path outFile, String rootName) throws Exception{
        KeyPair trustRootKeyPair = loadOrCreate(rootName);
        byte[] bytes = Serializer.instance().serialize(createIssuerPackage(trustRootKeyPair), "password");
        Files.write(outFile, bytes);
    }

    private KeyPair loadOrCreate(String rootName) throws Exception{
        Path pubKeyOut = Paths.get(rootName + ".pub");
        Path privKeyOut = Paths.get(rootName + ".key");

        if(Files.exists(pubKeyOut) && Files.exists(privKeyOut)) {
            PublicKey pubKey = CryptoUtils.instance().parsePublicKey(Files.readAllBytes(pubKeyOut));
            PrivateKey privKey = CryptoUtils.instance().parsePrivateKey(Files.readAllBytes(privKeyOut));
            return new KeyPair(pubKey, privKey);
        }

        KeyPair newKeyPair = CryptoUtils.instance().generate();

        Files.write(pubKeyOut, newKeyPair.getPublic().getEncoded());
        Files.write(privKeyOut, newKeyPair.getPrivate().getEncoded());
        return newKeyPair;
    }

    private IssuerPackage createIssuerPackage(KeyPair trustRootKeyPair) {
        KeyPair issuerKeyPair = CryptoUtils.instance().generate();

        byte[] issuerKeySignature = CryptoUtils.instance().sign(issuerKeyPair.getPublic().getEncoded(), trustRootKeyPair.getPrivate());
        byte[] trustRootId = CryptoUtils.instance().hash(trustRootKeyPair.getPublic().getEncoded());

        return new IssuerPackage( //
                issuerKeyPair.getPublic(), //
                issuerKeyPair.getPrivate(), //
                issuerKeySignature, //
                trustRootId
        );
    }
}
