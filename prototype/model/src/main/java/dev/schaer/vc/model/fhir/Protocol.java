package dev.schaer.vc.model.fhir;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import dev.schaer.vc.model.fhir.presets.Selection;

import java.util.List;

@JsonPropertyOrder({"targetDisease", "doseNumberPositiveInt", "seriesDosesPositiveInt"})
public class Protocol {
    public static final List<Selection<Protocol>> AVAILABLE = List.of(
            new Selection<>("1/1", new Protocol(1, 1, List.of(TargetDisease.COVID_19)))
            , new Selection<>("2/2", new Protocol(2, 2, List.of(TargetDisease.COVID_19)))
            , new Selection<>("2/3", new Protocol(2, 3, List.of(TargetDisease.COVID_19)))
            , new Selection<>("3/3", new Protocol(2, 2, List.of(TargetDisease.COVID_19)))
    );
    private int doseNumberPositiveInt;
    private int seriesDosesPositiveInt;
    private List<TargetDisease> targetDisease;

    public Protocol() {
        // default for Jackson
    }

    public Protocol(int doseNumberPositiveInt, int seriesDosesPositiveInt, List<TargetDisease> targetDisease) {

        this.doseNumberPositiveInt = doseNumberPositiveInt;
        this.seriesDosesPositiveInt = seriesDosesPositiveInt;
        this.targetDisease = targetDisease;
    }

    @Override
    public String toString() {
        return String.format("Protocol{%d/%d}", doseNumberPositiveInt, seriesDosesPositiveInt);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Protocol protocol = (Protocol) o;

        if (doseNumberPositiveInt != protocol.doseNumberPositiveInt) return false;
        if (seriesDosesPositiveInt != protocol.seriesDosesPositiveInt) return false;
        return targetDisease != null ? targetDisease.equals(protocol.targetDisease) : protocol.targetDisease == null;
    }

    @Override
    public int hashCode() {
        int result = doseNumberPositiveInt;
        result = 31 * result + seriesDosesPositiveInt;
        result = 31 * result + (targetDisease != null ? targetDisease.hashCode() : 0);
        return result;
    }

    public int getDoseNumberPositiveInt() {
        return doseNumberPositiveInt;
    }

    public void setDoseNumberPositiveInt(int doseNumberPositiveInt) {
        this.doseNumberPositiveInt = doseNumberPositiveInt;
    }

    public int getSeriesDosesPositiveInt() {
        return seriesDosesPositiveInt;
    }

    public void setSeriesDosesPositiveInt(int seriesDosesPositiveInt) {
        this.seriesDosesPositiveInt = seriesDosesPositiveInt;
    }

    public List<TargetDisease> getTargetDisease() {
        return targetDisease;
    }

    public void setTargetDisease(List<TargetDisease> targetDisease) {
        this.targetDisease = targetDisease;
    }
}
