package dev.schaer.vc.serialization;

import java.util.zip.Deflater;
import java.util.zip.Inflater;

public class CompressionUtils {
    private static final int BUF_SIZE = 4096;

    public static byte[] deflate(byte[] input) {
        byte[] buffer = new byte[BUF_SIZE];
        Deflater deflater = new Deflater(Deflater.BEST_COMPRESSION);
        deflater.setInput(input);
        deflater.finish();
        int outputSize = deflater.deflate(buffer);
        deflater.end();
        byte[] output = new byte[outputSize];
        System.arraycopy(buffer, 0, output, 0, outputSize);
        System.out.printf("%d -> %d = %.2f compression ratio",
                input.length,
                output.length,
                (1.0 * output.length) / input.length);
        return output;
    }

    public static byte[] inflate(byte[] input) {
        Inflater inflater = new Inflater();
        try {
            inflater.setInput(input);
            byte[] buffer = new byte[BUF_SIZE];
            int outputSize = inflater.inflate(buffer);
            byte[] output = new byte[outputSize];
            System.arraycopy(buffer, 0, output, 0, outputSize);
            return output;
        } catch (Exception e) {
            throw new RuntimeException("Unable to inflate bytes", e);
        } finally {
            inflater.end();
        }
    }
}
