package dev.schaer.vc.serialization.cbor;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import dev.schaer.vc.crypto.CryptoUtils;

import java.io.IOException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;

public class PrivateKeyDeserializer extends JsonDeserializer<PrivateKey> {

    @Override
    public PrivateKey deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        return CryptoUtils.instance().parsePrivateKey(jsonParser.getBinaryValue());
    }

    @Override
    public Class<PrivateKey> handledType() {
        return PrivateKey.class;
    }
}
