package dev.schaer.vc.serialization.cbor;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.security.PrivateKey;

public class PrivateKeySerializer extends JsonSerializer<PrivateKey> {

    @Override
    public void serialize(PrivateKey value, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeBinary(value.getEncoded());
    }

    @Override
    public Class<PrivateKey> handledType() {
        return PrivateKey.class;
    }
}
