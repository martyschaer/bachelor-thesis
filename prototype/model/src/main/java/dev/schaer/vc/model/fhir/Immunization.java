package dev.schaer.vc.model.fhir;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.time.LocalDate;

@JsonPropertyOrder({"status", "patient", "recorded", "lotNumber", "vaccineCode", "manufacturer", "resourceType", "protocolApplied", "occurenceDateTime"})
public class Immunization {
    private final String status = "completed";
    private final String resourceType = "Immunization";
    private Vaccine vaccineCode;
    private Patient patient;
    private LocalDate recorded;
    private LocalDate occurenceDateTime;
    private Organization manufacturer;
    private String lotNumber;
    private Protocol protocolApplied;

    public Immunization() {
        // default constructor for Jackson
    }

    public Immunization(Vaccine vaccineCode, Patient patient, LocalDate recorded, LocalDate occurenceDateTime, Organization manufacturer, String lotNumber, Protocol protocolApplied) {
        this.vaccineCode = vaccineCode;
        this.patient = patient;
        this.recorded = recorded;
        this.occurenceDateTime = occurenceDateTime;
        this.manufacturer = manufacturer;
        this.lotNumber = lotNumber;
        this.protocolApplied = protocolApplied;
    }

    @JsonIgnore
    public boolean isValid() {
        if (protocolApplied.getDoseNumberPositiveInt() < protocolApplied.getSeriesDosesPositiveInt()) {
            return false;
        }

        if(recorded.isBefore(occurenceDateTime)) {
            return false;
        }

        if(occurenceDateTime.isBefore(LocalDate.now().minusDays(365))) {
            return false;
        }
        if(!Vaccine.ACCEPTED.contains(vaccineCode)) {
            return false;
        }
        if(!Organization.ACCEPTED.contains(manufacturer)) {
            return false;
        }

        // if all else matches
        return true;
    }

    public String getStatus() {
        return status;
    }

    public String getResourceType() {
        return resourceType;
    }

    public Vaccine getVaccineCode() {
        return vaccineCode;
    }

    public void setVaccineCode(Vaccine vaccineCode) {
        this.vaccineCode = vaccineCode;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public LocalDate getRecorded() {
        return recorded;
    }

    public void setRecorded(LocalDate recorded) {
        this.recorded = recorded;
    }

    public LocalDate getOccurenceDateTime() {
        return occurenceDateTime;
    }

    public void setOccurenceDateTime(LocalDate occurenceDateTime) {
        this.occurenceDateTime = occurenceDateTime;
    }

    public Organization getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(Organization manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getLotNumber() {
        return lotNumber;
    }

    public void setLotNumber(String lotNumber) {
        this.lotNumber = lotNumber;
    }

    public Protocol getProtocolApplied() {
        return protocolApplied;
    }

    public void setProtocolApplied(Protocol protocolApplied) {
        this.protocolApplied = protocolApplied;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Immunization that = (Immunization) o;

        if (!status.equals(that.status)) return false;
        if (!resourceType.equals(that.resourceType)) return false;
        if (vaccineCode != null ? !vaccineCode.equals(that.vaccineCode) : that.vaccineCode != null) return false;
        if (patient != null ? !patient.equals(that.patient) : that.patient != null) return false;
        if (recorded != null ? !recorded.equals(that.recorded) : that.recorded != null) return false;
        if (occurenceDateTime != null ? !occurenceDateTime.equals(that.occurenceDateTime) : that.occurenceDateTime != null)
            return false;
        if (manufacturer != null ? !manufacturer.equals(that.manufacturer) : that.manufacturer != null) return false;
        if (lotNumber != null ? !lotNumber.equals(that.lotNumber) : that.lotNumber != null) return false;
        return protocolApplied != null ? protocolApplied.equals(that.protocolApplied) : that.protocolApplied == null;
    }

    @Override
    public int hashCode() {
        int result = status.hashCode();
        result = 31 * result + resourceType.hashCode();
        result = 31 * result + (vaccineCode != null ? vaccineCode.hashCode() : 0);
        result = 31 * result + (patient != null ? patient.hashCode() : 0);
        result = 31 * result + (recorded != null ? recorded.hashCode() : 0);
        result = 31 * result + (occurenceDateTime != null ? occurenceDateTime.hashCode() : 0);
        result = 31 * result + (manufacturer != null ? manufacturer.hashCode() : 0);
        result = 31 * result + (lotNumber != null ? lotNumber.hashCode() : 0);
        result = 31 * result + (protocolApplied != null ? protocolApplied.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Immunization{" +
                "\n\tvaccineCode=" + vaccineCode +
                ",\n\tpatient=" + patient +
                ",\n\trecorded=" + recorded +
                ",\n\toccurenceDateTime=" + occurenceDateTime +
                ",\n\tmanufacturer=" + manufacturer +
                ",\n\tlotNumber='" + lotNumber + '\'' +
                ",\n\tprotocolApplied=" + protocolApplied +
                "\n}";
    }
}
