package dev.schaer.vc.model.fhir;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

public class Identifier {
    private String value;

    public Identifier() {
        // default for Jackson
    }

    public Identifier(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "ID(" + value + ')';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Identifier that = (Identifier) o;

        return value != null ? value.equals(that.value) : that.value == null;
    }

    @Override
    public int hashCode() {
        return value != null ? value.hashCode() : 0;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
