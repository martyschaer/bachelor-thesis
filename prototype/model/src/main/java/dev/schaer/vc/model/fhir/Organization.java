package dev.schaer.vc.model.fhir;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import dev.schaer.vc.model.fhir.presets.Selection;

import java.util.List;
import java.util.Set;

@JsonPropertyOrder({"type", "display", "identifier"})
public class Organization {
    private static final String SYSTEM = "https://spor.ema.europa.eu/v1/organisations";
    public static final Selection<Organization> MODERNA = new Selection<>("Moderna", new Organization(new Identifier("ORG-100031184"), "Moderna Biotech Spain S.L."));
    public static final Selection<Organization> BIONTECH = new Selection<>("BioNTech", new Organization(new Identifier("ORG-100030215"), "Biontech Manufacturing GmbH"));
    public static final Selection<Organization> JANSSEN = new Selection<>("Janssen", new Organization(new Identifier("ORG-100001417"), "Janssen-Cilag International"));
    public static final Selection<Organization> ASTRAZENECA = new Selection<>("AstraZeneca", new Organization(new Identifier("ORG-100001699"), "AstraZeneca AB"));
    public static final List<Selection<Organization>> AVAILABLE = List.of(MODERNA, BIONTECH, JANSSEN, ASTRAZENECA);
    public static final Set<Organization> ACCEPTED = Set.of(MODERNA.getValue(), BIONTECH.getValue(), JANSSEN.getValue());

    private final String type = "Organization";
    private Identifier identifier;
    private String display;

    public Organization() {
        // default constructor for Jackson
    }

    public Organization(final Identifier identifier, final String display) {
        this.identifier = identifier;
        this.display = display;
    }

    public String getType() {
        return type;
    }

    public Identifier getIdentifier() {
        return identifier;
    }

    public void setIdentifier(Identifier identifier) {
        this.identifier = identifier;
    }

    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Organization that = (Organization) o;

        if (type != null ? !type.equals(that.type) : that.type != null) return false;
        if (identifier != null ? !identifier.equals(that.identifier) : that.identifier != null) return false;
        return display != null ? display.equals(that.display) : that.display == null;
    }

    @Override
    public int hashCode() {
        int result = type != null ? type.hashCode() : 0;
        result = 31 * result + (identifier != null ? identifier.hashCode() : 0);
        result = 31 * result + (display != null ? display.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Org(" + display + ')';
    }
}
