package dev.schaer.vc.model.certificate;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import dev.schaer.vc.model.fhir.Immunization;

import java.nio.charset.StandardCharsets;
import java.security.PublicKey;
import java.util.Arrays;

@JsonPropertyOrder({"data", "issuerKey", "dataSignature", "issuerKeySignature", "trustRootIdentifier"})
public class VaccinationCertificate {
    public static final String MAGIC_BYTES_AS_STRING = "vaccCERT";
    public static final byte[] MAGIC_BYTES = MAGIC_BYTES_AS_STRING.getBytes(StandardCharsets.UTF_8);

    private Immunization data;
    private byte[] dataSignature;
    private PublicKey issuerKey;
    private byte[] issuerKeySignature;
    private String trustRootIdentifier;

    public VaccinationCertificate() {
        // default for Jackson
    }

    public VaccinationCertificate(Immunization data, byte[] dataSignature, PublicKey issuerKey,
                                  byte[] issuerKeySignature, String trustRootIdentifier) {

        this.data = data;
        this.dataSignature = dataSignature;
        this.issuerKey = issuerKey;
        this.issuerKeySignature = issuerKeySignature;
        this.trustRootIdentifier = trustRootIdentifier;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        VaccinationCertificate that = (VaccinationCertificate) o;

        if (!data.equals(that.data)) return false;
        if (!Arrays.equals(dataSignature, that.dataSignature)) return false;
        if (!issuerKey.equals(that.issuerKey)) return false;
        if (!Arrays.equals(issuerKeySignature, that.issuerKeySignature)) return false;
        return trustRootIdentifier.equals(that.trustRootIdentifier);
    }

    @Override
    public int hashCode() {
        int result = data.hashCode();
        result = 31 * result + Arrays.hashCode(dataSignature);
        result = 31 * result + issuerKey.hashCode();
        result = 31 * result + Arrays.hashCode(issuerKeySignature);
        result = 31 * result + trustRootIdentifier.hashCode();
        return result;
    }

    public Immunization getData() {
        return data;
    }

    public void setData(Immunization data) {
        this.data = data;
    }

    public byte[] getDataSignature() {
        return dataSignature;
    }

    public void setDataSignature(byte[] dataSignature) {
        this.dataSignature = dataSignature;
    }

    public PublicKey getIssuerKey() {
        return issuerKey;
    }

    public void setIssuerKey(PublicKey issuerKey) {
        this.issuerKey = issuerKey;
    }

    public byte[] getIssuerKeySignature() {
        return issuerKeySignature;
    }

    public void setIssuerKeySignature(byte[] issuerKeySignature) {
        this.issuerKeySignature = issuerKeySignature;
    }

    public String getTrustRootIdentifier() {
        return trustRootIdentifier;
    }

    public void setTrustRootIdentifier(String trustRootIdentifier) {
        this.trustRootIdentifier = trustRootIdentifier;
    }

    public static class Builder {
        private Immunization data;
        private byte[] dataSignature;
        private PublicKey issuerKey;
        private byte[] issuerKeySignature;
        private String trustRootIdentifier;

        public Builder data(Immunization data) {
            this.data = data;
            return this;
        }

        public Builder dataSignature(byte[] dataSignature) {
            this.dataSignature = dataSignature;
            return this;
        }

        public Builder issuerKey(PublicKey issuerKey) {
            this.issuerKey = issuerKey;
            return this;
        }

        public Builder issuerKeySignature(byte[] issuerKeySignature) {
            this.issuerKeySignature = issuerKeySignature;
            return this;
        }

        public Builder trustRootIdentifier(String trustRootIdentifier) {
            this.trustRootIdentifier = trustRootIdentifier;
            return this;
        }

        public VaccinationCertificate build() {
            return new VaccinationCertificate(
                    data,
                    dataSignature,
                    issuerKey,
                    issuerKeySignature,
                    trustRootIdentifier
            );
        }
    }
}