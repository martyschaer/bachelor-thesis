package dev.schaer.vc.crypto;

import dev.schaer.vc.model.fhir.Identifier;

import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Arrays;

public class IssuerPackage {
    private PublicKey pubKey;
    private PrivateKey privKey;
    private byte[] pubKeySignature;
    private byte[] trustRootId;

    public IssuerPackage() {
        // default for Jackson
    }

    public IssuerPackage(PublicKey pubKey, PrivateKey privKey, byte[] pubKeySignature, byte[] trustRootId) {
        this.pubKey = pubKey;
        this.privKey = privKey;
        this.pubKeySignature = pubKeySignature;
        this.trustRootId = trustRootId;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IssuerPackage that = (IssuerPackage) o;

        if (!pubKey.equals(that.pubKey)) return false;
        if (!privKey.equals(that.privKey)) return false;
        if (!Arrays.equals(pubKeySignature, that.pubKeySignature)) return false;
        return Arrays.equals(trustRootId, that.trustRootId);
    }

    @Override
    public int hashCode() {
        int result = pubKey.hashCode();
        result = 31 * result + privKey.hashCode();
        result = 31 * result + Arrays.hashCode(pubKeySignature);
        result = 31 * result + Arrays.hashCode(trustRootId);
        return result;
    }

    public PublicKey getPubKey() {
        return pubKey;
    }

    public void setPubKey(PublicKey pubKey) {
        this.pubKey = pubKey;
    }

    public PrivateKey getPrivKey() {
        return privKey;
    }

    public void setPrivKey(PrivateKey privKey) {
        this.privKey = privKey;
    }

    public byte[] getPubKeySignature() {
        return pubKeySignature;
    }

    public void setPubKeySignature(byte[] pubKeySignature) {
        this.pubKeySignature = pubKeySignature;
    }

    public byte[] getTrustRootId() {
        return trustRootId;
    }

    public void setTrustRootId(byte[] trustRootId) {
        this.trustRootId = trustRootId;
    }
}
