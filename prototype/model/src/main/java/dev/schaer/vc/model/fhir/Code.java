package dev.schaer.vc.model.fhir;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({"code", "system"})
public class Code {

    private String system;
    private String code;

    public Code() {
        // default for Jackson
    }

    public Code(String system, String code) {
        this.system = system;
        this.code = code;
    }

    @Override
    public String toString() {
        return "Code(" + code + ')';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Code code1 = (Code) o;

        if (system != null ? !system.equals(code1.system) : code1.system != null) return false;
        return code != null ? code.equals(code1.code) : code1.code == null;
    }

    @Override
    public int hashCode() {
        int result = system != null ? system.hashCode() : 0;
        result = 31 * result + (code != null ? code.hashCode() : 0);
        return result;
    }

    public String getSystem() {
        return system;
    }

    public void setSystem(String system) {
        this.system = system;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
