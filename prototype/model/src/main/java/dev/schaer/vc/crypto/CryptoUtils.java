package dev.schaer.vc.crypto;

import dev.schaer.vc.model.certificate.VaccinationCertificate;
import dev.schaer.vc.serialization.Serializer;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.spec.ECGenParameterSpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

public class CryptoUtils {
    private static CryptoUtils INSTANCE;
    private static final String KEY_PAIR_GEN_ALG = "EC";
    private static final String EC_CURVE = "secp256r1";
    private static final String SIG_ALG = "SHA256withECDSA";
    private static final String MD_ALG = "SHA-256";
    private static final String SYM_ALG_DETAIL = "AES/CBC/PKCS5Padding";
    private static final String KEY_DERIV_ALG = "PBKDF2WithHmacSHA256";
    private static final String SYM_ALG = "AES";

    private static final byte[] SALT = "SodiumChloride".getBytes(StandardCharsets.UTF_8); // this is fine because we change the password and contents every time.
    private static final byte[] IV_VAL = "InitializationVe".getBytes(StandardCharsets.UTF_8); //TODO is this OK?
    private static final IvParameterSpec IV = new IvParameterSpec(IV_VAL);

    private final KeyPairGenerator generator;
    private final Signature ecdsa;
    private final MessageDigest messageDigest;
    private final KeyFactory keyFactory;
    private final Cipher symmetricCipher;


    private CryptoUtils() throws NoSuchAlgorithmException, InvalidAlgorithmParameterException, NoSuchPaddingException, InvalidKeySpecException {
        generator = KeyPairGenerator.getInstance(KEY_PAIR_GEN_ALG);
        generator.initialize(new ECGenParameterSpec(EC_CURVE), new SecureRandom());

        ecdsa = Signature.getInstance(SIG_ALG);

        messageDigest = MessageDigest.getInstance(MD_ALG);

        keyFactory = KeyFactory.getInstance(KEY_PAIR_GEN_ALG);

        symmetricCipher = Cipher.getInstance(SYM_ALG_DETAIL);
    }

    public static void renew() {
        try {
            INSTANCE = new CryptoUtils();
        } catch (NoSuchAlgorithmException | InvalidAlgorithmParameterException | NoSuchPaddingException | InvalidKeySpecException ce) {
            throw new CryptoException("Failed to initialize CryptoUtils", ce);
        }
    }

    public static CryptoUtils instance() {
        if (INSTANCE == null) {
            try {
                INSTANCE = new CryptoUtils();
            } catch (NoSuchAlgorithmException | InvalidAlgorithmParameterException | NoSuchPaddingException | InvalidKeySpecException ce) {
                throw new CryptoException("Failed to initialize CryptoUtils", ce);
            }
        }
        return INSTANCE;
    }

    public KeyPair generate() {
        return generator.generateKeyPair();
    }

    public PublicKey parsePublicKey(byte[] keyBytes) throws CryptoException {
        try {
            return keyFactory.generatePublic(new X509EncodedKeySpec(keyBytes));
        } catch (InvalidKeySpecException e) {
            throw new CryptoException("Unable to parse public key", e);
        }
    }

    public PrivateKey parsePrivateKey(byte[] keyBytes) throws CryptoException {
        try {
            return keyFactory.generatePrivate(new PKCS8EncodedKeySpec(keyBytes));
        } catch (InvalidKeySpecException e) {
            throw new CryptoException("Unable to parse private key", e);
        }
    }

    public byte[] encrypt(byte[] data, String password) throws CryptoException {
        try {
            synchronized (symmetricCipher) {
                symmetricCipher.init(Cipher.ENCRYPT_MODE, createSecretKey(password), IV);
                return symmetricCipher.doFinal(data);
            }
        } catch (IllegalBlockSizeException | BadPaddingException | NoSuchAlgorithmException | InvalidKeySpecException | InvalidKeyException | InvalidAlgorithmParameterException e) {
            throw new CryptoException("Unable to encrypt data", e);
        }
    }

    public byte[] decrypt(byte[] data, String password) throws CryptoException {
        try {
            synchronized (symmetricCipher) {
                symmetricCipher.init(Cipher.DECRYPT_MODE, createSecretKey(password), IV);
                return symmetricCipher.doFinal(data);
            }
        } catch (IllegalBlockSizeException | BadPaddingException | NoSuchAlgorithmException | InvalidKeySpecException | InvalidKeyException | InvalidAlgorithmParameterException e) {
            throw new CryptoException("Unable to decrypt data", e);
        }
    }

    private SecretKeySpec createSecretKey(String password) throws NoSuchAlgorithmException, InvalidKeySpecException {
        SecretKeyFactory skf = SecretKeyFactory.getInstance(KEY_DERIV_ALG);
        PBEKeySpec spec = new PBEKeySpec(password.toCharArray(), SALT, 65536, 256);
        return new SecretKeySpec(skf.generateSecret(spec).getEncoded(), SYM_ALG);
    }

    public byte[] sign(byte[] data, PrivateKey privateKey) throws CryptoException{
        try {
            synchronized (ecdsa) {
                ecdsa.initSign(privateKey, new SecureRandom());
                ecdsa.update(data);
                return ecdsa.sign();
            }
        } catch (InvalidKeyException | SignatureException e) {
            throw new CryptoException("Unable to sign data", e);
        }
    }

    public boolean verify(VaccinationCertificate cert, PublicKey trustRoot) {
        return verifyDetail(cert, trustRoot).valid();
    }

    public VerificationResult verifyDetail(VaccinationCertificate cert, PublicKey trustRoot) {
        VerificationResult.Builder builder = new VerificationResult.Builder();
        builder.publicKeyKnown(isMatchingTrustRoot(trustRoot, cert.getTrustRootIdentifier()));
        builder.issuerSignatureValid(verify(cert.getIssuerKey().getEncoded(), cert.getIssuerKeySignature(), trustRoot));
        builder.dataSignatureValid(verify(Serializer.instance().serialize(cert.getData()), cert.getDataSignature(), cert.getIssuerKey()));
        return builder.build();
    }

    private boolean isMatchingTrustRoot(PublicKey expected, String actualId) {
        String trustRootId = byte2b64str(hash(expected.getEncoded()));
        return trustRootId.equals(actualId);
    }

    public boolean verify(byte[] data, byte[] signature, PublicKey publicKey) throws CryptoException{
        try {
            synchronized (ecdsa) {
                ecdsa.initVerify(publicKey);
                ecdsa.update(data);
                return ecdsa.verify(signature);
            }
        }catch (InvalidKeyException | SignatureException e) {
            throw new CryptoException("Unable to verify signed data", e);
        }
    }

    public byte[] hash(byte[] data) {
        return messageDigest.digest(data);
    }

    public String hash2b64str(byte[] data) {
        return byte2b64str(hash(data));
    }

    public String byte2b64str(byte[] bytes) {
        return Base64.getEncoder().encodeToString(bytes);
    }

    public byte[] b64str2byte(String b64) {
        return Base64.getDecoder().decode(b64);
    }

    public String bytesToHex(byte[] bytes) {
        StringBuilder builder = new StringBuilder(bytes.length * 2);
        for(byte b : bytes) {
            builder.append(String.format("%02X ", b));
        }
        return builder.toString();
    }

}
