package dev.schaer.vc.model.fhir;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

@JsonPropertyOrder({"test", "coding"})
public class TargetDisease {
    public static TargetDisease COVID_19 = new TargetDisease(
            List.of(new Code("http://snomed.info/sct", "840539006")), "COVID-19"
    );

    private List<Code> coding;
    private String text;

    public TargetDisease() {
        // default for Jackson
    }

    public TargetDisease(List<Code> coding, String text) {
        this.coding = coding;
        this.text = text;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TargetDisease that = (TargetDisease) o;

        if (coding != null ? !coding.equals(that.coding) : that.coding != null) return false;
        return text != null ? text.equals(that.text) : that.text == null;
    }

    @Override
    public int hashCode() {
        int result = coding != null ? coding.hashCode() : 0;
        result = 31 * result + (text != null ? text.hashCode() : 0);
        return result;
    }

    public List<Code> getCoding() {
        return coding;
    }

    public void setCoding(List<Code> coding) {
        this.coding = coding;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
