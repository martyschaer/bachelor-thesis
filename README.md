# Bachelor Thesis - Digital Vaccination Certificates
## Topic
This thesis looks at digital vaccination certificates.
During the [Project 2](https://gitlab.com/martyschaer/project-2)
in the spring semester of 2021 we looked at the theory behind these
certificates and designed our own.

In this thesis I expand on that work by building an implementation
and doing a more in-depth comparison to the existing solutions
which by now have taken hold in the world.

## Index
- [`output/report.pdf`](https://gitlab.com/martyschaer/bachelor-thesis/-/blob/main/output/report.pdf) - the main project report
- [`media/defense.pdf`](https://gitlab.com/martyschaer/bachelor-thesis/-/blob/main/media/defense.pdf) - the presentation used during the defense
- [`media/short_presentation.pdf`](https://gitlab.com/martyschaer/bachelor-thesis/-/blob/main/media/short_presentation.pdf) - the short presentation used during the finaltag
- [`media/book.pdf`](https://gitlab.com/martyschaer/bachelor-thesis/-/blob/main/media/book.pdf) - the entry for [book.bfh.ch](https://book.bfh.ch)
- [`media/poster.pdf`](https://gitlab.com/martyschaer/bachelor-thesis/-/blob/main/media/poster.pdf) - a poster for display at the finaltag
- [`prototype`](https://gitlab.com/martyschaer/bachelor-thesis/-/tree/main/prototype) - the prototype implementation

### Compilation
The report can be compiled using `latexmk -pdf -pvc`.

## Other information
- Advisors:
  * Dr. Annett Laube
  * Dr. Reto Koenig
- Expert: Mathis Marugg
- Start Date: 2021-09-20
- End Date: 2022-01-20
