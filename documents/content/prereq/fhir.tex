\section{FHIR}
\label{sec:fhir}
In order to construct a proof of vaccination, the act of immunizing
(e.g. by vaccination) a person must be represented in digital form.
As discussed in the Project 2 that preceded this thesis,
\Gls{fhir} is ideally suited to this task. \citep[Section 4.1.1]{proj2}

\Gls{fhir}, the Fast Healthcare Interoperability Standard (pronounced \enquote{fire}),
is a standard by Health Level Seven International (HL7)
that describes an API and data format for exchanging health data.
The latest version of \Gls{fhir}, 4.0.1, is the first normative version of the
standard, released in 2019. \cite{fhirVersion}
\Gls{fhir} is organized by \enquote{resources} that represent entities, processes,
and other concepts that are relevant in the medical field.
The \enquote{Immunization} resource contains all the data
required for a proof of vaccination. \cite{fhirimmu}.

The Immunization resource could include information about the reason an
Immunization was performed or not performed,
who performed the immunization (down to a single healthcare worker),
what quantity of the vaccine was administered,
how the patient was educated,
and even the reactions to the immunization that the patient experienced.
While these are important data points in a medical context,
they are not needed for a vaccination certificate and are thus left out
(the standard declares many elements as optional).

An important component of \Gls{fhir} is the coding system and value sets.
To gain maximal compatibility there exist, for example, Europe wide value sets
which specify how different concepts are mapped into the resources.
\autoref{lst:vc_code_system_example} shows an example of such a coded value
(\texttt{"vaccineCode"}).
It specifies which vaccine was used in the immunization represented by this resource.
In this example it's the Moderna COVID-19 Vaccine.
The \texttt{coding} specifies both a \texttt{system} and a \texttt{code}.
The \texttt{system} in this case is the
\enquote{EU Public Health - Union Register of medicinal products}
which assigns an identifier
(e.g. \enquote{EU/1/20/1507} for the Spikevax COVID-19 vaccine)
to every medicinal product approved by the European Union. \cite{healthRegister}
Each coded value also contains either a \texttt{"text"} or \texttt{"display"}
element, which specifies a human readable text to be displayed instead of
the identifier.

The EU has published a separate document containing the value sets to be used
to represent vaccines, manufacturers, and diseases
in the \Gls{dgc}. \citep[Section 2.3]{dgcValueSets}

\begin{figure}[ht]
\thinlines
        \lstinputlisting[caption={Excerpt of a \Gls{fhir} record specifying a vaccine},label={lst:vc_code_system_example}]{listings/vc_code_system_example.json}
\end{figure}

\subsection*{Example}
There follows a description of each attribute in an Immunization resource
that is relevant to a proof of vaccination,
based on the complete example of an Immunization resource in
\autoref{lst:vc_data_example}

\begin{itemize}
        \item The \texttt{"status"} element is required by \Gls{fhir}, but is
                usually \texttt{"completed"} in a certificate.
                Could also be used to indicate
                that an immunization was not performed, together with
                the \enquote{statusReason} attribute.
        \item The \texttt{"resourceType"} element is always \texttt{"Immunization"},
                indicating the type of \Gls{fhir} resource.
        \item The \texttt{"vaccineCode"} element indicates which vaccination was used.
        \item The \texttt{"patient"} element links the Immunization resource to the person
                subject to this immunization (i.e. the person that received the vaccine and holder of the certificate).
        \item The \texttt{"recorded"} element indicates when the certificate was issued.
        \item The \texttt{"occurenceDateTime"} element indicates when the immunization took place.
        \item The \texttt{"manufacturer"} element indicates who manufactures or markets the vaccine that was used.
        \item The \texttt{"lotNumber"} contains the vaccine production lot number used.
        \item The \texttt{"protocolApplied"} specifies how the immunization was performed,
                indicating how many doses were given (\texttt{"doseNumberPositiveInt"})
                and how many are required (\texttt{"seriesDosesPositiveInt"}).
                This is because different patients (e.g. people who have had COVID-19)
                require different numbers of doses to complete their immunization.
                This also allows for flexibility with e.g. booster shots.
                The \texttt{"targetDisease"} is always COVID-19 in this context.
                Multiple \texttt{"targetDisease"}s can be recorded,
                because some vaccines (e.g. the MMR vaccine)
                target multiple diseases (e.g. measles, mumps, rubella).  \end{itemize}

\begin{figure}[ht]
\thinlines
\lstinputlisting[caption={Example of an Immunization resource},label={lst:vc_data_example}]{listings/vc_data_example.json}
\end{figure}
