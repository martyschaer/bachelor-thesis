\section{Chain of Trust}
\label{sec:chain_of_trust}
Digital signatures enable verifying that a given message
was written by a known sender (authenticity),
and that that message wasn't tampered with (integrity).
This concept is fundamental to any proof of vaccination system,
as the problem boils down to:
A certificate holder (\emph{Charlie}) claims to a verifier (\emph{Victor})
to be vaccinated by a doctor (\emph{Bob}),
by presenting a certificate (proof of vaccination).
To verify that,
A) \emph{Bob} issued this certificate (authenticity) and
B) \emph{Charlie} did not alter it (integrity)
a digital signature is used.
\emph{Victor} checks the validity of the certificate signature
against the known public key of \emph{Bob}.
If the signature is valid, \emph{Victor} can be reasonably sure that
\emph{Bob} created the certificate and that the proof of vaccination is valid.
This essentially creates a \enquote{Chain of Trust} (\Gls{cot})
from \emph{Bob} to the certificate,
i.e. iff \emph{Victor} knows and trusts \emph{Bob}, and the signature is valid,
the certificate must be valid.

Given \emph{Charlie's} vaccination record $V_{c}$,
\emph{Bob's} key pair \kp{r} (consisting of the public key \kpu{r} and the
private key \kpv{r}) and an identifier for \kp{r}, $H_{R} \equiv \textrm{hash}(K_{R}^{+})$
the proof of vaccination $C$ is constructed as follows:

\[
\begin{aligned}
        S_{V_{c}} &= \textrm{sign}(V_{c}, K_{R}^{-}) \\
        C &= H_{R} + S_{V_{c}} + V_{c} \\
\end{aligned}
\]

To verify the validity of the proof of vaccination $C$,
\emph{Victor} uses $H_{R}$ to look up \emph{Bob's} public key \kpu{r}
in his list of known and trusted certificate issuers,
then checks the authenticity and integrity of $V_{c}$ by verifying
the signature: 
\[
        \textrm{verify}(S_{V_{c}}, K_{R}^{+})
\]

In the above example, \emph{Bob} is the ultimate trust root of the certificate
for \emph{Victor}.
In that case, the \Gls{cot} has a single link
(i.e. \emph{Bob's} signature of the certificate).

However, it may be desirable to extend the \Gls{cot} so that \emph{Bob}
is not directly issuing proofs of vaccination.
Consider the following example, in which \emph{Bob} authorizes
\emph{Alice} to create proofs of vaccination.
By making use of the \Gls{cot}, it is sufficient that
\emph{Victor} knows and trusts \emph{Bob} to verify a proof of vaccination
issued by \emph{Alice}.
In addition to the previously given $V_{c}$, \kp{r}, and $H_{R}$ also consider
\emph{Alice'} key pair \kp{a} and an \enquote{authorization signature} $SA_{A}$
such that \emph{Bob} signs \emph{Alice'} public key \kpu{a}:
\[
        SA_{A} = \textrm{sign}(K_{A}^{+}, K_{R}^{-})
\]

A proof of vaccination $C$ with a longer Chain of Trust can now be constructed as follows:

\[
\begin{aligned}
        S_{V_{c}} &= \textrm{sign}(V_{c}, K_{A}^{-}) \\
        C &= H_{R} + SA_{A} + K_{A}^{+} + S_{V_{c}} + V_{c}
\end{aligned}
\]

To verify the validity of the proof of vaccination $C$,
\emph{Victor} uses $H_{R}$ to look up \emph{Bob's} public key \kpu{r}
in his list of known trust anchors,
then checks the authenticity and integrity of \kpu{a} by verifying
the authorization signature: 
\[
        \textrm{verify}(SA_{A}, K_{R}^{+})
\]

This establishes that \emph{Bob} has authorized \emph{Alice}
to issue proofs of vaccination.
\emph{Victor} then uses an identical process as in the previous example 
to verify the authenticity and integrity of $V_{c}$:
\[
        \textrm{verify}(S_{V_{c}}, K_{A}^{+})
\]

Thus we have created a chain of trust with three links
as illustrated in \autoref{fig:chain_of_trust},
where $\textrm{A} \equiv H_{R}$,
$\textrm{B} \equiv SA_{A}$,
$\textrm{C} \equiv K_{A}^{+}$,
$\textrm{D} \equiv S_{V_{c}}$,
and $\textrm{E} \equiv V_{c}$.

This shows that iff \emph{Victor} can rely on the trust anchor $\textrm{A}$,
he can establish that the vaccination record $\textrm{E}$ can be trusted.
If however, any link in the \Gls{cot} breaks (i.e. a signature is invalid),
the presented proof of vaccination is no longer valid.

\begin{figure}[htp]
  \centering
  \includegraphics[width=.6\textwidth]{chain_of_trust.png}
  \caption{Chain of Trust (\Gls{cot})}
  \label{fig:chain_of_trust}
\end{figure}
