\section{Identification}
\label{sec:identification}
A proof of vaccination must be securely linked to the certificate holder
in order to ensure that it cannot be copied and used by another person.

Many systems make use of a passport or
national Identity Document (\Gls{id}) card that certificate holders
must present alongside the certificate itself to establish the link between
the proof of vaccination and the person. \cite{sccTech}
The passport or \Gls{id} are well suited to this, because forgery
or use of a document not applying to person carrying it are illegal, 
thus the penalty for simply taking e.g. a sibling's ID and certificate
is quite severe (up to three years in prison and a fine).
\cite{idForgery}

There are different ways of linking a certificate to an ID.
Many systems choose to include the full name (i.e. first and last name)
and the date of birth on the certificate.
During verification, the information on the certificate must then be compared
to the presented ID.
Another possibility is the \enquote{document number}.
Each \Gls{id} is assigned a unique document number, which can
be included in the certificate instead of the full name and date of birth.

\subsection{Machine Readability}
\label{subsec:machine_readability}
The Swiss passport and \Gls{id} card are both examples of
Machine Readable Travel Documents (\Gls{mrtd}) as specified in the
International Civil Aviation Organization's (\Gls{icao})
document 9303. \cite{icao9303}
The \Gls{icao} is, among other things, a standards body creating standards
relating to air travel.
It is important to note that the \Gls{icao} does not itself have any regulatory
authority. It makes recommendations, which member states may then choose to enforce.
\cite{icao}

The \Gls{icao} document 9303 stipulates that all \Glspl{mrtd}
must have a Machine Readable Zone (\Gls{mrz}).
An \Gls{mrz} is designed to be easily read using machine vision,
thus a font (OCR-B), position, size, and it's contents
are well defined. \citep[part 3, section 4]{icao9303}
The exact format of an \Gls{mrz} depends on the size of travel document
(i.e. passport booklets are TD3, whereas \Gls{id} cards are TD1).
\citep[parts 4, 5]{icao9303}
Since TD1 and TD3 sized \Glspl{mrtd} cover the majority of Swiss citizens,
the thesis will focus on those.

An \Gls{mrz} contains the name, date of birth, and gender
of the \Gls{id} holder, as well as the document type, document number,
and expiration date
(usually the same information that is also contained in human-readable
format elsewhere on the \Gls{id})
Check digits are used to ensure that the information was read correctly.
\citep[part 3, section 4.9]{icao9303}

An example of a \Gls{mrtd} of size TD1 is given in \autoref{fig:mrz_example}.

\begin{figure}[htp]
  \centering
  \includegraphics[width=.5\textwidth]{mrz_example.png}
  \caption{\Gls{mrtd} Example (\Gls{mrz} is the white area)}
  \label{fig:mrz_example}
\end{figure}

\subsection{Security}
\label{subsec:id.security}
Identity documents often have multiple levels of security to protect against
forgery. Some of these features enable a layperson to detect a forgery,
while others may require forensic analysis.
\Gls{icao} document 9303 part 2 states the mandatory and optional security features
of \Glspl{mrtd} and how to detect and verify them.
\cite{icao9303}

These features may involve the material an \Gls{mrtd} is made from
being difficult to obtain,
manufacturing techniques,
or even cryptographic signatures on e-\Glspl{id}.

An inspection in visible wave-lengths may provide defense against
the most basic forgery techniques, but more advanced readers
usually scan an \Gls{mrtd} in the infra-red, visible, and ultra-violet spectrum.
