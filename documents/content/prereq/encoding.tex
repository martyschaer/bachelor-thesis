\section{Encoding}
\label{sec:encoding}
\subsection{Objects}
\label{subsec:objects}
To transport structured data it must be encoded or serialized.
For the use case of a proof of vaccination encoding objects is especially interesting,
as a vaccination record is a structure of nested objects (see \autoref{sec:fhir}).

One way to serialize an object for transport is using
JavaScript Object Notation (JSON) as defined in ECMA-404. \cite{ecma404}
JSON is a subset of JavaScript, and supports two different modes of nesting:
the first is objects (aka. dictionaries or key-value pairs)
which map a name or key to a value
(a value can be a string, number, boolean, null, object, or array)
the second is arrays, which contain a list of values
(as described before).

JSON has the advantage of being widely supported due to the popularity
of JavaScript, but it also has some disadvantages:
firstly, it does not support values of binary data,
which would be useful when transporting cryptographic material.
Secondly, it is fairly verbose (i.e. space in-efficient),
which can be prohibitive in low-bandwidth communication.

\Gls{cbor}, the Concise Binary Object Representation,
as defined in RFC 8949, provides a solution to both of these downsides.
\citep{rfcCbor}
It is based on the JSON data model,
supporting the same value types and nesting modes,
but also supports binary data as a value type.
Additionally, \Gls{cbor} is designed to provide fairly small message sizes.
\cite{cborio}
A JSON encoded object only contains printable characters,
while a \Gls{cbor} encoded object is a byte stream that also contains non-printable characters.
This leads to the main downside, that an object encoded as \Gls{cbor} is
no longer human readable,
but that can be mitigated by use of diagnostic tools such as
\href{https://cbor.me/}{cbor.me}.

An important feature of \Gls{cbor} for the purpose of cryptographically signing \Gls{cbor} objects is
that the encoded representation of objects (called \enquote{maps} in \Gls{cbor}) has
a well defined, deterministic key order.
According to the standard,
\blockquote[{\citep[Section 4.2.1]{rfcCbor}}]{The keys in every map MUST be sorted in the bytewise lexicographic order of their deterministic encodings.}.
This makes it possible and convenient to not only check signature validity against
the exact bytes an object was delivered as,
but also against a re-encoded version of that object with identical data contents.

The CBOR Object Signing and Encryption (\Gls{cose}) extension on the \Gls{cbor}
standard specifies how to sign and/or encrypt objects using \Gls{cbor}.
It was inspired by the JSON Object Signing and Encryption (JOSE) standard,
but takes advantage of the above described features of \Gls{cbor}.
\Gls{cose} also defines a message structure to be used.
\cite{rfcCose}

\subsection{2D Barcodes}
\label{subsec:2dbarcodes}
A 2D barcode, as opposed to a 1D barcode, encodes data in two dimensions.
The most prominent example of this is the Quick Response (QR) Code,
which was developed in the 1990s by Denso Wave. \cite{qrcode}
Others include Aztec Code, Data Matrix, and PDF417.
Denso Wave has patented the QR-Code, but allows it to
be used free of charge as long as users follow the specification.
\cite{qrPatent}

QR Codes have many different uses, but they all rely on transmitting
some piece of information to the user via a camera.
They can be many different sizes (called \enquote{Versions}),
which determine the amount of data that can be contained,
from Version 1 (21x21 pixels) to Version 40 (177x177 pixels).
\cite{qrsize}
QR Codes make use of different levels of error correction,
which allows them to be read even if they are partially damaged or obscured
(e.g. torn paper, reflection on screen).
The error correction level is configurable from L, M, Q, to H which allow
\SI{7}{\percent},
\SI{15}{\percent},
\SI{25}{\percent}, and
\SI{30}{\percent}
of data bytes respectively to be recovered.
\cite{qrec}

QR Codes also offer different encoding modes, such that certain limited
types of information can be stored more efficiently.
The following examples are at error correction level L:
a QR Code can contain \SI{2.953}{\kilo\byte} of binary data,
7089 digits of numerical information,
and 4296 characters in \enquote{alphanumeric} mode which are restricted to:
the capital letters \texttt{A-Z},
the digits \texttt{0-9}, and 9 special characters
(i.e. \texttt{\$ \% * + - . / : <space>}).
\citep[section 7.4.4]{qriso}

\subsection{Binary}
\label{subsec:binary}
Many communication channels (notably 2D barcodes and JSON),
do not support direct binary data.
The binary data must usually be encoded in such a way,
that it only contains printable characters (or even a subset of those).
The most popular such encoding is Base64 as specified in RFC 4648.
It uses 64 characters of the ASCII alphabet
(\texttt{[A-Za-z0-9+/]} and a special padding character \texttt{=})
to represent 6 bits each, such that 3 bytes with 8 bits ($3 * 8 = 24$ bits)
map to 4 characters with 6 bits ($4 * 6 = 24$ bits).
The output is padded with \texttt{=}, such that is it always a multiple of 4 characters.
The standard also specifies a URL- and filesystem-safe version,
called \enquote{Base64url},
which uses \texttt{-} and \texttt{\_} instead of \texttt{+} and \texttt{/}
respectively.
\cite{rfcBase64}.

Recently a variation of Base64, Base45, has been specified for use
with QR and Aztec codes.
It essentially works the same as Base64, but uses a more restricted set of 
the 45 characters allowed in a QR Codes alphanumeric mode, encoding 2 bytes per 3 characters.
It is still in the draft stage and shouldn't be used or referenced because
it isn't stable yet.
\cite{rfcBase45}

\newpage

Despite that, it has been widely implemented in the different proof of
vaccination systems, because it allows a more efficient encoding of data.
This can be shown by multiplying the capacity of the QR Code,
which differs by mode,
with the efficiency of the encoding ($\frac{\textrm{bits in}}{\textrm{bits out}}$):

\[
\begin{aligned}
  \textrm{binary mode with Base64} &= 2953 \times \frac{18}{24} \approx \SI{2214}{\byte} \\
  \textrm{binary mode with Base45} &= 2953 \times \frac{16}{24} \approx \SI{1968}{\byte} \\
  \textrm{alphanumeric mode with Base45} &= 4296  \times \frac{16}{24} \approx \SI{2864}{\byte} \\
\end{aligned}
\]

It is obvious that Base45 is a less efficient encoding ($\approx 0.67$)
than Base64 ($\approx 0.75$),
but that is canceled out by the ability of a QR Code to store
more ($1.45 \times$) symbols in alphanumeric mode than in binary mode.

