\section{Comparison}
\label{sec:comparison}
This section draws a comparison between
the Swiss Covid Certificate (\Gls{scc}) system,
the fictional centralized CENT system,
and my proposed \Gls{vc} system
described previously,
in terms of the data the certificates contain,
the processes used,
the availability and privacy of the system,
as well as any special features.
In \autoref{sec:comp_summary} these comparisons are summarized in a compact format.

\subsection*{Data}
In terms of vaccination data all three systems process roughly the same information:
which vaccine was used (and the associated manufacturer),
when the vaccination took place,
number of doses administered,
number of doses scheduled,
and the lot number.

They all contain some method to link the certificate to the Holder.
In case of the \Gls{scc} it is the full name and date of birth.
The CENT links the Holder directly to the vaccination
in the centralized vaccination database.
The \Gls{vc} contains the identification document number to link
an \Gls{mrtd} to the certificate, which can be used to identify the Holder.

\subsection*{Processes}
The processes for Issuing are different between the three systems.
The CENT makes use of a centralized vaccination database,
meaning the person receiving the vaccine is automatically known
to have received it.
In the \Gls{scc}, a centralized service is used to issue certificates,
either through an API for other health systems (e.g. vacme.ch) or a web interface.
The cantons authorize persons allowed to issue certificates.
The \Gls{vc} has a different approach to the other two:
by having a central Trust Root authorizing Issuers,
the Issuers can then issue certificates locally without the involvement of
the Trust Root.

The CENT only requires the Holder to presented an ID to the Verifier,
while the \Gls{scc} and \Gls{vc} require the Holder to additionally
present a QR Code.
An image or printout of the QR Code suffices, but the \Gls{scc} offers
a wallet app.

To verify a certificate in the \Gls{scc} and \Gls{vc} systems,
the Verifier scans the QR Code and is told if the certificate is authentic
and if the business rules are met (i.e. number of doses, date).
In the \Gls{scc}, the Verifier must then manually compare the full name and
date of birth to an \Gls{id} presented by the holder,
while in the \Gls{vc} system the Verifier scans the \Gls{mrz} on the
\Gls{mrtd} presented by the holder, and the ID link is established automatically.
In the CENT system, the Verifier scans the ID of the Holder
and contacts a central service with the ID information
to establish the validity of the certificate.

In terms of convenience and usability, both \Gls{vc} and CENT ensure
a correct verification through automation, even if the Verifier is lazy.
The \Gls{scc} requires manually checking several pieces of information
on the \Gls{id}, which may get tiring after performing many verifications.

Revocation in the \Gls{scc} can be done on a certificate-by-certificate basis
using the \Gls{uvci} that's included in each certificate,
whereas the \Gls{vc} only allows revoking entire Issuers
(in case they become untrustworthy).
This has the consequence that potentially many certificates would need
to be re-issued when an Issuer is revoked in the \Gls{vc} system.
To revoke a CENT certificate, the vaccination record is
deactivated or deleted from the central database.

\subsection*{Availability}
The centralized nature of the CENT and \Gls{scc} mean that
both rely on a connection to the central service in order to issue certificates,
the \Gls{vc} only requires a connection for the first-time setup of an Issuer.
Afterwards, certificates can be issued locally.

Both the \Gls{scc} and \Gls{vc} rely on the Verifiers having a periodic connection
to the certificate and issuer revocation lists respectively,
but a connection is not needed to verify every certificate.
In contract to that, the CENT requires a connection to the central service
for every verification.

\subsection*{Privacy}
In terms of health data, both the \Gls{scc} and CENT make use of a centralized
service to issue certificates. This means that health data are not only
kept between the patient and the administering healthcare worker,
but is also shared with the central authority.
The \Gls{vc} system keeps the health data between the
patient and the administering healthcare worker,
because they issue certificates directly and locally.

With the \Gls{scc} and \Gls{vc} the Verifier has access to the vaccination information
at the time of verification. The exception to this is the Certificate Light
included in the \Gls{scc}.
The CENT does not expose the vaccination information to the Verifier.

If an \Gls{scc} certificate is lost or stolen,
it can be traced back to a person fairly easily due to the inclusion of the
full name and date of birth. Thus, this information is potentially exposed
without the knowledge of the Holder.
If an \Gls{vc} certificate is lost or stolen,
the vaccination information is linked to the pseudonymous identification document
number.
The Issuing entity of the identity document can break this pseudonymity.

With all three systems it is possible for a malicious
group of Verifiers to build movement profiles and social graphs of the
certificate Holders, by remembering their \Gls{id} or other identifying
features of the certificates and correlating that by location and time.
This is also possible for the central authority in the CENT system,
while the \Gls{scc} and \Gls{vc} prevent this by verifying certificates locally.

\subsection*{Special Features}
The \Gls{scc} offers the Certificate Light as a privacy friendlier variant
of the normal certificate.
However, requirements for \texttt{2G/2G+/Testing} make the Certificate Light
less relevant because the validity of a certificate in one of those modes
reveals information about the contents (vaccinated/tested).
Verification of light certificates is not possible when using the aforementioned
modes in the official verifier app.

The \Gls{vc} enables automated access to restricted areas (such as air travel)
through the link with a Machine Readable Travel Document (\Gls{mrtd}).
While is is a theoretical possibility with the \Gls{scc}, it was not implemented.
