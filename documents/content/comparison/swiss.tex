\section{Swiss Covid Certificate}
The Swiss Covid Certificate is mostly an implementation of the European Union
Digital Covid Certificate.
This was done in order to have easy compatibility with the neighboring countries.
The system documentation and implementation are available on GitHub \cite{swissCovCertDocs}.

\subsection*{Design}
The Swiss Covid Certificate not only supports proof of vaccination,
but also proof of recovery and proof of test result.
This means it has some features that are not relevant for proof of vaccination
(such as test result transfer codes or \texttt{3G/2G/2G+} verification)
that won't be further explored here.
This first part describes the design elements that are shared with the
\Gls{dgc}

As the design document for the \Gls{dgc} states:
\blockquote[{\citet[p.4]{dgcTrust}}]{
The verifier of a certificate should be able to establish that: 
\begin{itemize}
  \item The certificate has been issued by an authorized entity
  \item The information presented on the certificate is authentic, valid, and has not been altered
  \item The certificate can be linked to the holder of the certificate
\end{itemize}
}

Furthermore, it states in the main design principles and business requirements:
\blockquote[{\citet[p.4--6]{dgcTrust}}]{
\begin{itemize}
  \item Cross-border interoperability
  \item Data protection \textelp{}
  \item Data security and privacy by design and by default
  \item Inclusiveness (especially medium-neutrality)
  \item Modularity and scalability
  \item Open standards
  \item \textelp{}
\end{itemize}
}

With these goals in mind, the solution proposed by the EU is cryptographic signing
with a public key sharing infrastructure to facilitate cross-border interoperability.
Privacy and data protection are ensured by using a decentralized system,
where each certificate carries the data locally,
not sharing health records and personal data in a centralized way.
This approach also scales well: by making use of the PKI, member countries
can determine their own systems for issuing certificates
or assigning signing-privileges,
as long as they share the signing keys with the other members.

Medium agnosticism is achieved through use of 2D barcodes (QR Codes),
which can either be displayed on a screen or on augmented paper
\blockquote[{\citet[p.5]{dgcTrust}}]{
(i.e. paper certificate with printed machine-
readable parts such as barcodes, QR Codes, Machine Readable Zones)
}

\subsection*{Certificate Contents}
A \Gls{dgc} consists of a document which contains the vaccination data
which is wrapped in a container to ensure integrity and authenticity.
As shown in in \autoref{fig:dgc_format_drawing} the vaccination data
is first encoded in the \Gls{cbor} format.
The \Gls{cbor} document is then signed using \Gls{cose}
and compressed.
Finally it's Base45 encoded and a QR Code is created from that text.

\begin{figure}[htp]
  \centering
  \includegraphics[width=.7\textwidth]{dgc_format_drawing.png}
  \caption{\Gls{dgc} data format summary}
  \label{fig:dgc_format_drawing}
\end{figure}

The \enquote{Metadata} in the diagram alludes to the PKI,
meaning validator apps know the trusted signing keys.

The vaccination document of a \Gls{dgc} certificate is best explained using the example shown in
\autoref{lst:dgc_example} \citet[\texttt{simple.json}]{dgcExample}
Essentially, this format is a re-mapping of the information contained in a
\Gls{fhir} Immunization record to a simpler, less nested structure.

The \texttt{nam} element encodes
the given (\texttt{gn}) and family (\texttt{fn}) name
of the carrier of the certificate,
both as human readable text, and as it would be shown on the \Gls{mrz} on
an \Gls{mrtd}
Together with the date of birth (\texttt{dob}), this identifies the carrier
in combination with an official identification document.

The \texttt{v} element contains the vaccination information.
This is where, instead of the \texttt{v}, a \texttt{t} or \texttt{r}
element could be placed to
create a certificate for testing or recovery respectively.

\begin{itemize}
        \item\texttt{tg} - TarGet disease (always COVID-19)
        \item\texttt{vp} - Vaccine or Prophylaxis used
        \item\texttt{mp} - Medical Product approval code
        \item\texttt{ma} - Manufacturer of the vaccine
        \item\texttt{dn} - Dose Number - the number of doses administered
        \item\texttt{sd} - Series Doses - the number of doses required
        \item\texttt{dt} - DaTe of vaccination
        \item\texttt{co} - COuntry of vaccination
        \item\texttt{is} - ISsuer of the certificate
        \item\texttt{ci} - Certificate Identifier (\Gls{uvci})
\end{itemize}

The Unique Vaccination Certificate Identifier (\Gls{uvci}) has multiple purposes.
The most often used, is that individual \Glspl{dgc} can be revoked
(e.g. if the name is misspelled or it is discovered that a certificate was
fraudulently issued).

However, it could also be used to verify certificates manually,
such as over the phone if a \enquote{non-augmented paper} version of a certificate is presented.
In that case the system is essentially centralized, because \Glspl{uvci} need to
be verified with the issuing member state.
To enable this, a \Gls{uvci} can come in one of three flavours.
Common to all three is that they begin with a version and country indicator
and end with a check sum.
The first flavour is the most modular, containing sections for the
issuing entity, vaccine and an opaque unique id.
The second only contains an opaque unique id,
and the third contains a section for the issuing entity
and an opaque unique string.
Member states are free to decided how they encode and generate the various sections,
but the \Gls{uvci} should not contain any personally identifying information
and may only consist of alphanumeric characters.
\cite{dgc}

\begin{figure}[ht]
\thinlines
\lstinputlisting[caption={official DGC example},label={lst:dgc_example}]{listings/dgc_example.json}
\end{figure}

\subsection*{Swiss Specializations}
What the certificate looks like and what it contains cannot be customized,
as that is part of the \Gls{dgc} specification that every member country must
implement if they want to take part.

The Swiss system makes uses of a centralized signing service,
where certificates can either be issued through a web interface by authorized
persons or through an API by \Glspl{his} of the Cantons (e.g. vacme.ch).

The centralized service can issue a temporary (valid for \SI{48}{\hour})
\enquote{Certificate Light} (CL).
The CL is only valid in Switzerland, as it is incompatible with the \Gls{dgc} specification
and contains only the full name, date of birth, and cryptographic signatures,
but no medical data.
This has two advantages: it does not expose the certificate Holder's health information
and the QR Code is smaller, thus easier to read.
To obtain a CL, the Holder uploads their full certificate to the centralized service
and in turn receives the CL.
This can be done through the Swiss Covid Certificate Wallet app.

\autoref{fig:swiss_system_diagram} shows the simplified data flow in the Swiss Covid Certificate system.
The red-dashed arrows represent actions which require a remote-connection
between participants, while the blue-solid arrows show actions which
either require no connection or only a local (face to face) one.

\begin{enumerate}[A), leftmargin=1.5em]
        \item When a vaccination is administered, the issuer sends the vaccination
                details to the centralized signing service.
                The signing service then returns the complete \Gls{scc} certificate.
        \item The QR Code and the vaccination details in a human readable form are printed out
                and handed to the patient/holder.
                In many cases, the holder also receives their certificate in electronic form
                through the vaccination appointment system (i.e. \texttt{vacme.ch}).
        \item When prompted by a Verifier, the Holder must present both the QR Code
                and an official identity document (e.g. ID card, driver's license, SwissPass).
                The QR Code can either be on paper or a screen (e.g. the \Gls{scc} wallet app),
                making it technology agnostic.
        \item The Verifier uses the verification app to scan the QR Code,
                and if the \Gls{uvci} doesn't appear on the certificate revocation list.
                Then, the Verifier manually checks if the presented ID corresponds
                to the name and date of birth displayed by the verification app.
        \item The trust list (a list of public keys authorized to issue certificates)
                and certificate revocation list (a list of invalid \Gls{dgc} certificates)
                is regularly updated from the centralized EU repository.
        \item The Verifier must periodically download the trust list
                and the certificate revocation list to make sure they're up to date.
        \item If a certificate is invalid (as described previously), it's added
                to the certificate revocation list and no longer considered valid.
\end{enumerate}

\begin{figure}[ht]
  \centering
  \includegraphics[width=1\textwidth]{swiss_system_diagram}
  \caption{\Gls{scc} System Overview}
  \label{fig:swiss_system_diagram}
\end{figure}

The certificate Holder wallet app featured a prominent button
to refresh trust list and certificate revocation lists.
It also validated the current certificate, followed by an animation
featuring a green check mark.
This was sometimes used by Verifiers to verify certificates.
This is obviously unsound, because the Verifier trusts the phone
presented by the Holder instead of scanning the certificate QR Code
with a trusted device.
No instances of this design flaw being taken advantage of are known,
and the Issue is addressed as of January 2022.
\cite{githubRefreshButton}

\clearpage

\subsection{Information Distribution}
\autoref{tab:swiss.info_destrib} shows how various pieces of information are
distributed in the \Gls{scc} system.
It uses the notation introduced in \autoref{sec:chain_of_trust}
and \autoref{subsec:vc.info_destrib}
for the different components of a Chain of Trust.
The common knowledge shared among all participants is the EU Trust List
and the Certificate Revocation List.

It's clear from this visualization, that the \Gls{id} and vaccination information $V_{c}$,
contained in $C$, are seen by the Central Authority, effectively creating
a centralized vaccination registry.

\begin{table}[htp]
        \centering
        \SetTblrInner{hlines, vlines, hspan=even}
        \begin{tblr}{% multicol configurations
                cell{3}{4}={c=2}{c},
                cell{4}{3}={c=2}{c},
                cell{6}{3}={c=3}{c},
                cell{7}{5}={c=2}{c},
                }
                \textbf{ID} & \textbf{Step} & \textbf{Central Authority} & \textbf{Issuer} & \textbf{Holder} & \textbf{Verifier} \\
                A & Initial & & & \Gls{id} & \\
                B & Vaccinate Patient & & $V_{c}$, \Gls{id} & & \\
                C & Enter Data & $V_{c}$, ID & & & \\
                C & Issue Certificate & $C = S_{V_{c}} + V_{c}$ & & & \\
                D & Hand out Certificate & $C$ & & & \\
                E & Verify Certificate & & & $C$, \Gls{id} & \\
        \end{tblr}
        \caption{Information Distribution with \Gls{scc}}
        \label{tab:swiss.info_destrib}
\end{table}

