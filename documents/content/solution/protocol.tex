\section{Protocol}
\label{sec:vc_protocol}
A proof of vaccination in the \Gls{vc} system takes the form of a container
that contains the following five sections ( as shown in \autoref{lst:vc_container_example}),
that together form a Chain of Trust as described in \autoref{sec:chain_of_trust}.

\begin{itemize}
        \item The \texttt{"data"} element contains a \Gls{fhir} Immunization resource
                (see \autoref{sec:fhir}) as a nested object.
        \item The \texttt{"dataSignature"} element is a digital signature
                by the Issuer that ensures the authenticity and integrity of the
                \texttt{"data"} element.
                The input to the signature is the \Gls{cbor}
                encoded (see \autoref{subsec:objects}) Immunization resource,
                signed by the Issuer's private key.
        \item The \texttt{"issuerKey"} element is the X.509 encoded public key of the Issuer.
        \item The \texttt{"authorizationSignature"} element is a digital signature
                by the Trust Root that authorizes the Issuer's key pair to be
                used for issuing. It's input is the X.509 encoded public key of the Issuer
                (same as the \texttt{"issuerKey"} element),
                signed by the Trust Root's private key.
        \item The \texttt{"trustRootIdentifier"} element is a Base64 encoding of 
                the SHA-256 hash of the Trust Root's public key.
                This is used by the Issuer to identify which Trust Root generated this certificate.
\end{itemize}

\begin{figure}[htp]
  \thinlines
  \lstinputlisting[caption={\Gls{vc} Container Format},label={lst:vc_container_example}]{listings/vc_container_example.json}
\end{figure}

The cryptography used for these signatures is based on Elliptic Curves (EC),
specifically the Elliptic Curve Digital Signature Algorithm \Gls{ecdsa},
because EC cryptography provides good security at with short key lengths.
\cite{nsa}
This is important when transmitting such cryptographic material through
low-bandwidth channels i.e. 2D barcodes.

\subsection{Encoding}
\label{subsec:vc.proto.encoding}
To hold and present an \Gls{vc}, the container is packaged into a convenient
QR Code that can either be presented on a piece of paper or a device with a screen,
as long as the side length of the QR Code is at least \SI{30}{\milli\meter}.

The encoding of the QR Code takes place in the \enquote{pipeline} shown in \autoref{fig:vc_qr_pipeline}:

\begin{figure}[ht]
  \centering
  \includegraphics[width=1\textwidth]{vc_qr_pipeline.png}
        \caption{\Gls{vc} to QR Code pipeline}
  \label{fig:vc_qr_pipeline}
\end{figure}

\begin{enumerate}
        \item The given \Gls{vc} container (as previously described)
                is first \Gls{cbor} \emph{serialized}.
        \item The resulting binary stream is the \emph{compressed},
                which reduces the already small \Gls{cbor} size
                by another \SI{15}{\percent} to \SI{\approx780}{\byte}.
                The exact size depends on the data in the Immunization resource
                and the random numbers used in \Gls{ecdsa}.
        \item The compressed binary stream is then Base64 \emph{encoded} for compatibility
                with QR Codes (see \autoref{subsec:binary}). \label{item:encode}
                At the end of this stage, the ASCII string \texttt{vaccCERT} is
                prefixed to the encoded string, to distinguish
                \Gls{vc} QR Codes from other QR Codes and make the scanning more convenient.
        \item The QR Code is generated in binary mode, using error correction level L
                (see \autoref{subsec:2dbarcodes}).

\end{enumerate}

The Base64 encoding described in step \ref{item:encode} was chosen because of
how well supported it is.
A more efficient variant of this pipeline would use Base45 instead of Base64 
and generate the QR Code in alphanumeric mode,
for the reasons described in \autoref{subsec:binary}.
Another variant would put the compressed binary stream into the QR Code in binary
mode without any encoding to printable characters,
but this is unsupported in several popular QR Code libraries.

\subsection{Issuer Revocation}
\label{subsec:vc.proto.revoke}
The issuer revocation list contains an entry per line which are formatted as follows:
\[
        \texttt{<issuer>;<signature>}
\]
Where the \texttt{<issuer>} is the Issuer Id, a Base64 encoded SHA-256
hash of the issuer public key (similar to the Trust Root Identifier),
and the \texttt{<signature>} is the Base64 encoded signature of the Issuer Id,
signed by the Trust Root.
The \Gls{irl} is made accessible to the Verifiers as a \Gls{utf8} encoded text
file served on a web server over \Gls{https}.

\subsection{Verification}
\label{subsec:vc.proto.verification}
To verify an \Gls{vc} the Verifier's App takes the following steps:

\begin{enumerate}
        \item Scan the presented QR Code and identification document (\Gls{mrtd})
        \item Decode it into the \Gls{vc} container by reversing the encoding steps \label{item:decode}
        \item Verify the authenticity and integrity of the \Gls{cot} as described in \autoref{sec:chain_of_trust} \label{item:verify}
        \item Match up the patient identifier in the \Gls{vc} and the scanned \Gls{mrtd} \label{item:match}
        \item Check any additional business rules (e.g. days since vaccination, type of vaccine, etc.) \label{item:rules}
\end{enumerate}

If anything unexpected occurs in steps
\ref{item:decode},
\ref{item:verify},
\ref{item:match}, or
\ref{item:rules},
such as an invalid signature or the wrong ID being presented,
the \Gls{vc} is rejected.
